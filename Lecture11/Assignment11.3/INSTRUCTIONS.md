## Assignment 11.3: Web App with Code

1. Install "Azure Accounts" and "Azure App Service" extensions to your VSCode.
2. Create an API like in the lecture example.
3. Create an Azure Web App for the API
4. Deploy the code to the web app
5. Profit! :)
6. Remove your application from Azure :(