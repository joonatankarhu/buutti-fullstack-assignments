## Assignment 11.4: Web App with Docker

1. Dockerize the API from the previous assignment.
2. Upload the image to Azure Container Registry.
3. Create an Azure Web App using the image.
4. Test that the service works.
5. Remove the Web App and associated resources.
