## Assignment 11.5: Web App with Gitlab

Following the lecture example, create a CI/CD pipeline that deploys a new version of your API every time new changes are committed and pushed.
