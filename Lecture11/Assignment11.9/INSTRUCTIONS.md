## Assignment 11.9: Deploy Calendar API via CI/CD

Create a Gitlab repository for the Calendar API. Enable continuous development in the Azure Web App. Create a CI/CD pipeline that builds and deploys the Calendar API.