## Assignment 11.7: Deploy Calendar API as Code

Create a new **Web App** in Azure and deploy the calendar API from the previous assignment as **code**.