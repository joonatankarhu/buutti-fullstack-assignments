## Assignment 11.2: Downtime

How much of yearly downtime is allowed for following services, before Microsoft will compensate your losses?

1. AKS (Azure Kubernetes Service)
2. App Service with Free tier
3. Azure DNS

A little help with calculations: https://uptime.is/

answer:

How much of yearly downtime is allowed for following services, before Microsoft will compensate your losses?

1. AKS Azure Kubernetes Service

< 99.95%	10%
< 99%	25%
< 95%	100%

2. No SLA is provided for the Free tier.

3. Azure DNS

< 100%	  10%
< 99.99%	  25%
< 99.5%   100%