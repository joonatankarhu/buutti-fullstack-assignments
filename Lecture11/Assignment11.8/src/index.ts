import express, { Request, Response } from 'express'
import 'dotenv/config'

// Calendar API

const app = express()
app.use(express.json())

const PORT = process.env.PORT

interface Event {
  id: string
  title: string
  description: string
  date: string
}

let events: Array<Event> = []

app.post('/', (req: Request, res: Response) => {
  const event = req.body
  events.push(event)
  res.status(201).send('Added new event')
})

app.put('/:eventId', (req: Request, res: Response) => {
  const eventId = parseInt(req.params.eventId)
  const updatedEvent: Event = req.body

  events = events.map((event) => {
    if (parseInt(event.id) === eventId) {
      return { ...event, ...updatedEvent }
    }
    return event
  })

  console.log(events)

  res.status(201).send('Updated event')
})

app.delete('/:eventId', (req: Request, res: Response) => {
  const eventId = req.params.eventId

  events = events.filter((event) => {
    event.id !== eventId
  })

  res.status(201).send('Deleted event')
})

app.get('/', (_req: Request, res: Response) => {
  res.json(events)
})

app.get('/:monthNumber', (req: Request, res: Response) => {
  const monthNumber = parseInt(req.params.monthNumber)

  const nextMonthEvents = events.filter((event) => {
    const eventDate = new Date(event.date)
    const eventMonth = eventDate.getMonth()

    if((eventMonth + 2) === monthNumber) {
      return event
    }
  })
  console.log(nextMonthEvents)

  res.status(201).send(nextMonthEvents)
})

app.listen(PORT || 3000, () => console.log('Listening on port 3000'))
