## Assignment 11.8: Deploy Calendar API as Container

Create a **Docker container** from the Calendar API. Test that it runs locally. Then create a new **Web App** in Azure and deploy the container.