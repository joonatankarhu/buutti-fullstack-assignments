## Assignment 11.6: Calendar API

Create a calendar API. It should have following endpoints:

- `GET /` returns a list of all events
- `GET /:monthNumber` returns a list of all the events in the next month given as parameter
- `POST /` adds an event to the calendar
- `PUT /:eventId` modifies the event corresponding to the eventId
- `DELETE /:eventId` deletes the event corresponding to the eventId

Events should have an id, title, description, date, and optionally a time. Store the events in a transient in-memory storage (i.e. a variable).