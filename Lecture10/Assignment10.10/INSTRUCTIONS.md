## Assignmnet 10.10: Secure Notice Board

Add secret note endpoints to your notice board. There should be endpoits to create, read, and delete secret notices. To access any of these endpoints user should need to provide a username and a password. 

Build an API that provides a protected endpoint. To access the endpoint the user should send a username and a password either on basic authentication header or request body. The endpoint should verify that the credentials are valid, and if so, serve the secret content. Remember that secret information needs to be provided via environment variables.

Create a Dockerfile for the app. Build the app image so that the environment variables are provided to Docker during the **build time**, resulting in a container that can be run without giving any environment variable information.

Run your application in a container. Make sure the secret is served if and only if the user provides correct username and password.
