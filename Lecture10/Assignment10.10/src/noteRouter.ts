import express, { Request, Response } from 'express'
import argon from 'argon2'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const router = express.Router()

interface Note {
  id: number
  note: string
}

let notes: Note[] = []

router.post('/', (req: Request, res: Response) => {
  const note = req.body
  notes.push(note)
  console.log(notes)
  res.status(201).send('Created a note')
})

router.get('/', (_req: Request, res: Response) => {
  console.log(notes)
  res.status(200).json(notes)
})

router.delete('/delete/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  notes = notes.filter((note) => note.id !== id)

  res.status(201).send('Deleted note')
})

export default router
