import express from 'express'
import 'dotenv/config'
import { authenticate } from './middleware'

import noteRouter from './noteRouter'
import userRouter from './userRouter'
import secretRouter from './secretEndpoint'

const app = express()
app.use(express.json())

app.use('/note', noteRouter)
app.use('/user', userRouter)
app.use('/secret', authenticate, secretRouter)

const PORT = process.env.PORT

app.listen(PORT || 3000, () => console.log('Listening on port 3000'))
