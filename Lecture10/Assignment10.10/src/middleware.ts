import { NextFunction, Request, Response } from 'express'
import 'dotenv/config'

export const authenticate = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const VALID_USERNAME = process.env.VALID_USERNAME ?? ''
  const VALID_PASSWORD = process.env.VALID_PASSWORD ?? ''
  
  const { username, password } = req.body

  const headersInput = JSON.stringify(req.headers)
  const parsed = JSON.parse(headersInput)
  const headersUsername = parsed.username
  const headersPassword = parsed.password

  if(username && password) {
    if (username === VALID_USERNAME && password === VALID_PASSWORD) {
      next()
    }
  }

  if(headersUsername && headersPassword) {
    if (headersUsername === VALID_USERNAME && headersPassword === VALID_PASSWORD) {
      next()
    }
  }

  return res.status(401).send('Unauthorized')
}
