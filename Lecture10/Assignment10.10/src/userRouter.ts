import express, { Request, Response } from 'express'
import argon from 'argon2'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const router = express.Router()

interface User {
  username: string
  password?: string
  hashPassword?: string
}

// eslint-disable-next-line no-var
const users: User[] = []

router.post('/register', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const hash = await argon.hash(password)

  const newUser = {
    username,
    hashPassword: hash,
  }

  // const token = jwt.sign({ username }, secret, { expiresIn: '1h' })
  users.push(newUser)
  res.send('Registered account')
})

// router.post('/login', async (req: Request, res: Response) => {
//   const { username, password } = req.body

//   const user = users.find((user) => user.username === username)

//   if (user === undefined) {
//     return res.send(401).send('Incorrect username')
//   }

//   try {
//     if (typeof user.hashPassword === 'undefined') {
//       return res.status(400).send('wrong type')
//     }
//     const passwordMatches = await argon.verify(user.hashPassword, password)
//   } catch (error) {
//     console.log(error)
//   }

//   // console.log('user logged in: ', user)

//   // const isValidPassword = await argon.verify(user.passwordHash, password)
//   // if (!isValidPassword) {
//   // }
//   res.send('Login')
// })

export default router
