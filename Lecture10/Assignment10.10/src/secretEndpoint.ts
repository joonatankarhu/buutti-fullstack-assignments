import express, { Request, Response } from 'express'
import 'dotenv/config'

const router = express.Router()
interface Note {
  id: number
  note: string
}

let secretNotes: Note[] = []

router.post('/', (req: Request, res: Response) => {
  const { id, note } = req.body
  const newNote: Note = {
    id,
    note
  }
  secretNotes.push(newNote)
  console.log(secretNotes)
  res.status(201).send('Created a secret note!')
})

router.get('/', (_req: Request, res: Response) => {
  console.log(secretNotes)
  res.status(200).json(secretNotes)
})

router.delete('/delete/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  secretNotes = secretNotes.filter((note) => note.id !== id)

  res.status(201).send('Deleted secret note')
})

export default router
