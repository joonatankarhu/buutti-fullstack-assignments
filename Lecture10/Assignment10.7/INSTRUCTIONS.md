## Assignment 10.7: Comparing Base Images

Write a Node.js application that prints a single line of text. Notice that you do not need a package.json file nor any dependencies for this. Create four Docker images of the application with
1. `node:latest` base image
2. `node:slim` base image
3. `node:alpine` base image
4. `alpine:latest` linux base image with Node and NPM installed with `RUN apk add --update nodejs npm`

All Node images of course have Node and NPM pre-installed. Alpine Linux is just a (very lightweight) linux distribution, so it does not come with Node and NPM. They need to be installed before Node programs can be executed.

Compare the sizes of the images. Why would you prefer one of these images over another?
