## Assignment 10.4: Using a Dockerfile

Better, automated way to create images is by using a *Dockerfile*.
Create an empty directory to hold your Dockerfile. In command line write: `mkdir myimage && cd myimage`.
Create the Dockerfile with your editor of choice. Repeat the earlier process, but this time with the Dockerfile to automate it; start with Ubuntu base image, run update on it and install figlet. 
Build an image from the Dockerfile with `docker build -t figlet .`

`-t` will create tag for image, named figlet
`.` indicates the location of the build context

Try running the image with `docker run -it figlet`