## Assignment 10.9: Notice Board

Build an API that represents a public notice board. The api should have endpoints for creating, reading, and deleting notes.

Add a Dockerfile and build an image of the app. Run the app in a Docker container.
