import express, { Request, Response } from 'express'
import 'dotenv/config'

const app = express()
app.use(express.json())

const PORT = process.env.PORT

interface Note {
  id: number,
  note: string
}

let notes: Note[] = []

app.post('/create-note', (req: Request, res: Response) => {
  const note = req.body
  notes.push(note)
  console.log(notes)
  res.status(201).send('Created a note')
})

app.get('/', (_req: Request, res: Response) => {
  console.log(notes)
  res.status(200).json(notes)
})

app.delete('/delete/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  notes = notes.filter((note) => note.id !== id)

  res.status(201).send('Deleted note')
})

app.listen(PORT || 3000, () => console.log('Listening on port 3000'))
