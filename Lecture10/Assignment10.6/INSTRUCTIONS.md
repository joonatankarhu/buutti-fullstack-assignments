## Assignment 10.6: Multi Stage Build

Create another image from your API used in the previous assignment. This time create the dockerfile using multi stage build. The Docker build process should include the TypeScript build stage.The finished image should not include any dev dependencies.

