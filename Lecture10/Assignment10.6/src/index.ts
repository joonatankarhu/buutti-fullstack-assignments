import express, { Request, Response } from 'express'

const app = express()

app.get('/hello', (_req: Request, res: Response) => {
  res.status(200).send('Hello from Docker!')
})

app.listen(3000, () => console.log('listening on port 3000'))

