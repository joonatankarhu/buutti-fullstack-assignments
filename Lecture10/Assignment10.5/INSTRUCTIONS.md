## Assignment 10.5: Dockerized Node App

1. Create a super simple Express app with one endpoint that returns "Hello World".
2. Create a Dockerfile and use that to create an image of your app
3. Run your app in a Docker container
4. Test the app with the tool of your choice
