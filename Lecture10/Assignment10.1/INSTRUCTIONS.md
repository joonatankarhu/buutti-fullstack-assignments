## Assignment 10.1: Intro to Docker

**Prerequisite**: Docker installed and ready to go.

Download and run our first container by entering the following in a command line. The flag `-it` stands for interactive terminal.
```docker run -it ubuntu```

Notice how it doesn’t find the image and resorts to getting one from **Docker Hub registry**. We now also have access to the terminal of the container.
Now we have our first image and container. We can later view them (in our own terminals) with the commands:
```
docker images
docker container ls
docker ps -a
```
