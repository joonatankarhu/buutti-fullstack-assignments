#### Assignment 10.3: Modifying Images

One way to do this is to do a `docker commit`, that saves all changes made to said container into a new layer and creates a new image.
Let’s start our Ubuntu with docker `run -it ubuntu` and prep it with `apt-get update && apt-get install figlet`.
Next we exit the container, and with `docker ps x-lq` we can extract the id of the container we just exited. New image can then be created from modified container with `docker commit <yourContainerId>`

Check the id of the newly created image with `docker image ls`. Try running it with `docker run -it <imageId>` and test Figlet. This method is used less than docker build, but it’s useful for smaller scale stuff.