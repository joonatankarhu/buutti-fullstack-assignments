import express, { Request, Response } from 'express'
import 'dotenv/config'

const app = express()
app.use(express.json())

const PORT = process.env.PORT

app.get('/counter', (_req: Request, res: Response) => {
  let number1 = 0
  let number2 = 1
  let nextTerm

  let counter = 0
  let intervalId = setInterval(() => {
    console.log(number1)
    nextTerm = number1 + number2
    number1 = number2
    number2 = nextTerm

    counter++
    res.send(number1)

    if (counter >= 15) {
      clearInterval(intervalId)
    }
  }, 1000)

  res.status(200)
})

app.listen(PORT || 3000, () => console.log('Listening on port 3000'))
