## Assignment 10.8: Whale Read Fibonacci

Write a program that, starting from zero, every second prints out the next number in the fibonacci sequence. Add a Dockerfile, build an image of the app, and run the image in a Docker container. Use multi stage build with TypeScript.