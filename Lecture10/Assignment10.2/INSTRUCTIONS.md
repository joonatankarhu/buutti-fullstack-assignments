## Assignment 10.2: Modifying Containers

Continuing from the last task, we now have a basic Ubuntu distro running in our new container. Lets run in the container terminal: `apt-get update` and install a small program to our Ubuntu with `apt-get install figlet`. 

Test the functionality with `figlet hello`.

If you `exit` the container now, and build it up straight after, Figlet won’t be there, as we modified the container, not the image it was based on. Next we want to create a new image with Figlet in it.
