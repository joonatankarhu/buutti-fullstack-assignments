## Assignment 7.8: Converter Pipeline

Create a Gitlab repository for the converter. Add ESLint and build a pipeline that lints and tests the code when it's pushed to the repository.
