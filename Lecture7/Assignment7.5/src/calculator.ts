export function calculator(
  operator: string,
  num1: number,
  num2: number
): number {
  if (operator === '+') {
    const sum = num1 + num2
    console.log(sum)
    return sum
  }
  if (operator === '-') {
    const sum = num1 - num2
    console.log(sum)
    return sum
  }
  if (operator === '/') {
    const sum = num1 / num2
    console.log(sum)
    return sum
  }
  if (operator === '*') {
    const sum = num1 * num2
    console.log(sum)
    return sum
  }
  if (
    operator !== '+' &&
    operator !== '-' &&
    operator !== '/' &&
    operator !== '*'
  ) {
    console.log('Cant do that!')
  }
}
