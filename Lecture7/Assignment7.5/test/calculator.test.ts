import { calculator } from '../src/calculator'
describe('Testing with division', () => {
  it('returns 3, if 9 12 divided by 4', () => {
    calculator('/', 12, 4)
  })
  it('testing 2 + 2 = 40', () => {
    calculator('+', 2, 2)
  })
  it('testing 2 - 2 = 0', () => {
    calculator('-', 2, 2)
  })
  it('check 5 times 5', () => {
    calculator('*', 5, 5)
  })
})