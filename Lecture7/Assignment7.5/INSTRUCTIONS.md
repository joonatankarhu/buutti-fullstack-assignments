## Assignment 7.5: Test Coverage

Run your calculator tests with --coverage flag. See what parts of your calculator are being tested and what are not.

Write more tests to get 100% coverage.
