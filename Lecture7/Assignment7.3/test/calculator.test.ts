import { calculator } from '../src/calculator'

test('test plus operator in calculator func', () => {
  expect(calculator('+', 2, 2)).toBe(4)
})

test('multiplication', () => {
  expect(calculator('*', 5, 5)).toBe(25)
})

test('invalid string input', () => {
  expect(calculator('-', '10', 6)).toBe(4)
})
