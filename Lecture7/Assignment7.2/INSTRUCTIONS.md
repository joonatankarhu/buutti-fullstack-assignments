
## Assignment 7.2: Azure Function Deployment

Create a new Azure Function App and **deploy** the function created in the previous assignment.

Test the function using Insomnia/Postman.

When finished, remove the function app and all associated resources.