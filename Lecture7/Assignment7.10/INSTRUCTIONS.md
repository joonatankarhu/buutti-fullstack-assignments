## Assignment 7.10: Cats and Dogs

You can get random dog images from [Dog CEO API](https://dog.ceo/dog-api/).
You can get random cat images from [Cat as a Service](https://cataas.com/).
You can get random fox images from [Random Fox](https://randomfox.ca/floof/)

Create and deployn an Azure function that returns randomly either a cat picture or a dog picture. But for every 13th request it returns a fox picture! The return format should be a JSON object with a message and a link to the picture. 

**Extra**: Make your function return HTML so that when used, the client sees the picture, and the message as a heading.