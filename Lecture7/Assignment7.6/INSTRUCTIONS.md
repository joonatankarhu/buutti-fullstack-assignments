## Assignment 7.6: Test Pipeline

Create a Gitlab project for the calculator.
Add ESLint and make sure the linter runs locally without issues
Add .gitlab-ci.yml file that
Has stages for installing, linting and testing
Has a common cache for node modules
Push your changes to Gitlab and make sure that all the stages run successfully.
