// const amount = process.argv[2]

export function convert(
  amount: number,
  unit: string,
  targetUnit: string
): number {
  const units = {
    // each unit in ml
    deciliter: 100,
    dl: 100,
    liter: 1000,
    ounce: 29.5735,
    oz: 29.5735,
    cup: 236.588,
    pint: 568.261,
  }

  const amountInUnit = amount * units[unit]

  const amountInTargetUnit = amountInUnit / units[targetUnit]
  console.log(amountInTargetUnit)

  const result = amountInTargetUnit
  return result
}

// convert(20, 'pint', 'oz')
