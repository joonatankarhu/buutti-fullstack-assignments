import { convert } from '../src/index'

describe('Convert units in ml to target units', () => {
  it('Convert dl to liter', () => {
    expect(convert(10, 'deciliter', 'liter')).toBe(1)
  })
  it('Convert dl to ounce', () => {
    expect(convert(6, 'deciliter', 'ounce')).toBe(20.28843390197305)
  })
  it('Convert liter to dl', () => {
    expect(convert(10, 'liter', 'deciliter')).toBe(100)
  })
  it('Convert cup to oz', () => {
    expect(convert(40, 'cup', 'oz')).toBe(320)
  })
  it('Convert pint to oz', () => {
    expect(convert(20, 'pint', 'oz')).toBe(384.30419125230355)
  })
})
