## Assignment 7.7: Command Line Converter

Write a command line unit converter that converts between volume units. Your converter should accept at least units deciliter, liter, ounce, cup, and pint.

The program takes three parameters: amount, source unit, and the target unit. For example
```convert 6 dl oz --> 20```

Write tests for your converter. Include at least five tests!
