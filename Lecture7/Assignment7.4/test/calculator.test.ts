import { calculator } from '../src/calculator'

describe('Testing with division', () => {
  it('returns 3, if 9 12 divided by 4', () => {
    calculator('/', 12, 4)
  })
  
  it('returns error, five must be a number type', () => {
    calculator('/', 10, 5)
  })

  it('testing big numbers', () => {
    calculator('/', 0.000001, 432493249)
  })
})
