## Assignment 7.4: Describe Syntax

Continue building your test suite to the calculator. Write tests for the division, this time using the describe/it syntax. Again, remember the “difficult” cases.
