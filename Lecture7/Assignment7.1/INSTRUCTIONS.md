
## Assignment 7.1: Azure Function Project

Following the lecture instructions, create a new Function Project with
- JavaScript as language
- HTTP trigger as template
- Anonymous authorization level

The function should expect a body parameter *input* of type *string*, and it should return that string transformed to upper case in the response body.

Test the function locally.
