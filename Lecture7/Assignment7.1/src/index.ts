import express from 'express'

const server = express()
server.use(express.json())

const { PORT } = process.env

server.listen(PORT, () => console.log('listening to port', PORT))
