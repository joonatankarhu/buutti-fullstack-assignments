## Assignment 7.9: Random Number Function

Create and deploy an Azure function that generates random numbers. The function should take three body parameters:
1. The minimum number (inclusive)
2. The maximum number (exclusive)
3. A boolean value that tells if the function should return an integer
