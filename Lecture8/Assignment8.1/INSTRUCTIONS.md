## Assignment 8.1: Simple Request & Response

Create a basic **HTTP-server application**. The application should respond to **GET** requests by returns a response with the following content: "Hello world!".

Test your application either with Postman, or by navigating your browser to `http://localhost:<your_portnumber>`

