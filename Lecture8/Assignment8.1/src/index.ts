import http, { IncomingMessage, ServerResponse } from 'http'

const server = http.createServer(
  (req: IncomingMessage, res: ServerResponse) => {
    if (req.method === 'GET') {
      res.write('Hello world!')
      res.end()
    }
  }
)

const port = 3000

server.listen(port)
console.log(`listening to port ${port}`)
