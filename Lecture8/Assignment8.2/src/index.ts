import express, { Request, Response } from "express";

const server = express();
const port = 3000;
server.get("/", (req: Request, res: Response) => {
  res.send("Hello World!!");
});

server.get("/get", (req: Request, res: Response) => {
  res.send("Hello again!");
});

server.listen(port, () => {
  console.log("Server running on port", port);
});
let user = "";

function greet() {
  console.count();
  return `hi ${user}`;
}

user = "bob";
greet();
user = "alice";
greet();
greet();
console.count();
