## Assignment 8.2: Simple Express Server

Create a basic **Express application**. The application should respond to **GET** requests by returns a response with the following content: "Hello world!".

**Extra**: Add a new endpoint to your application. So, in addition to accessing `http://localhost:3000`, try to send something back from `http://localhost:3000/endpoint2`.