
 ## Assignment 8.5: Logger Middleware

 Create a new project called **Student Registry**. We will be developing this program in stages during this lecture. For now it should have a single **GET** endpoint `/students` that returns an empty list.

 Create a **logger middleware** function that is used in all the project's endpoints. The middleware should log 
 1. the time the request was made
 2. the method of the request
 3. the url of the endpoint
 4. the request body

**Hint**: You need `express.json()` middleware to enable JSON body handling in express.
