import express, { Request, Response, NextFunction } from 'express'

const server = express()
const list = {}

const middleware = (req: Request, _res: Response, next: NextFunction) => {
  const time = new Date()
  const method = req.method
  const url = req.url
  console.log({ reqTime: time, reqMethod: method, reqUrl: url })

  next()
}

server.use(middleware)

server.get('/students', (_req: Request, res: Response) => {
  res.send(list)
})

server.listen(3000, () => {
  console.log('listening to port 3000')
})
