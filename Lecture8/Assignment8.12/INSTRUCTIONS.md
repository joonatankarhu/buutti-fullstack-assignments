
## Assignment 8.12: Improved Books API

1. Create validator middleware to POST and PUT endpoints that checks that all the required parameters are present.

1. Create a 404 Not Found -errorhandler and use it in the application
2. Create a middleware for logging in the application. Make the logs readable. req-object contains a lot of needed data for this. Use Google if you need ideas or suggestions of how to achieve this. Do not use any external packages.
3. Add [helmet](https://github.com/helmetjs/helmet) to your application’s dependencies and create a basic setup for security. 