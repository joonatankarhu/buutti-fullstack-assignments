import { Request, Response, NextFunction } from 'express'

export const notFound = (_req: Request, res: Response, _next: NextFunction) => {
  res.status(404).send('Not Found')
}

export const logging = (req: Request, _res: Response, next: NextFunction) => {
  const data = {
    date: new Date().toISOString(),
    method: req.method,
    headers: req.headers,
    params: req.params,
    query: req.query,
    url: req.url,
    request_body: req.body,
  }

  console.log(data)

  next()
}
