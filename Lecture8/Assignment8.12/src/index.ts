import express, { NextFunction, Request, Response } from 'express'
import helmet from 'helmet'
import { logging, notFound } from './middleware'

const app = express()

app.use(helmet())
app.use(express.json())

interface Book {
  id: number
  name: string
  author: string
  read: boolean
}

let books: Array<Book> = []

app.use(logging)

const validator = (req: Request, res: Response, next: NextFunction) => {
  const body = req.body
  const { name, author, read } = body
  if (!name || !author || !read) {
    res.status(400).send('Invalid or missing parameters!')
  } else {
    next()
  }
}

app.post('/api/v1/books', validator, (req: Request, res: Response) => {
  const newBook: Book = req.body

  books.push(newBook)

  res.status(201).send('Created a book')
})

app.put('/api/v1/books/:id', validator, (req: Request, res: Response) => {
  const id = parseInt(req.params.id)

  if (books.length > 0) {
    const newBook = req.body

    const findBookById = books.filter((book) => book.id === id)
    const book = findBookById[0]

    const editBook = (book: any, newBook: any): void => {
      if (book !== newBook) {
        book.name === newBook.name
          ? null
          : (book = { ...book, name: newBook.name })
        book.author === newBook.author
          ? null
          : (book = { ...book, author: newBook.author })
        book.read === newBook.read
          ? null
          : (book = { ...book, read: newBook.read })

        const index = books.findIndex((book) => book.id === id)
        if (index !== -1) {
          books[index] = book
          res.status(201).send(books)
        }
      } else {
        res.status(400).send('No changes were made')
      }
    }

    editBook(book, newBook)
  }
})

app.get('/api/v1/books', (_req: Request, res: Response) => {
  if (books.length < 1 || undefined) {
    res.status(404).send('Error: there are no books in the list...')
  }
  res.status(200).send(books)
})

app.get('/api/v1/books/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const targetBook = books.filter((book) => book.id === id)
  res.status(200).send(targetBook[0])
})

app.delete('/api/v1/books/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const index = books.findIndex((book) => book.id === id)
  if (index !== -1) {
    books = books.splice(index - 1, 1)
    console.log(books)
    res.status(200).send('Removed book')
  }
})

app.use(notFound)

app.listen(3000, () => console.log('Listening on port 3000'))
