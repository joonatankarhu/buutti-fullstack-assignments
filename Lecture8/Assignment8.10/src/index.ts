import express, { NextFunction, Request, Response } from 'express'

const app = express()
app.use(express.json())

interface Student {
  name: string
  score: number
}

type Students = Array<Student>

const validate = (req: Request, res: Response, next: NextFunction) => {
  const students: Students = req.body

  for (const student of students) {
    const { name, score } = student

    if (typeof name !== 'string' || typeof score !== 'number') {
      return res.status(400).send('Missing or invalid parameters!')
    }
  }

  next()
}

app.post('/scoreChecker', validate, (req: Request, res: Response) => {
  const students: Students = req.body
  let sum = 0

  for (const student of students) {
    const { name, score } = student
    sum = sum + score

    if (typeof name !== 'string' || typeof score !== 'number') {
      return res.status(400).send('Missing or invalid parameters!')
    }
  }

  const average = sum / students.length
  let highestScore = 0
  for (const student of students) {
    if (student.score > highestScore) {
      highestScore = student.score
    }
  }
  const highestScoreStudent = students.filter(
    (student) => student.score === highestScore
  )
  const higherOrEqual = students.filter((student) => student.score >= average)
  const percentage = `${(higherOrEqual.length / students.length) * 100}%`
  const results = {
    average_score: average,
    highest_score_students: highestScoreStudent,
    percentage_of_studs_higher_or_equal: percentage,
  }

  console.log(results)

  res.send(results)
})

const port = 3000

app.listen(port, () => console.log(`listening on port ${port}`))
