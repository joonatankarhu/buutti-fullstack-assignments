
## Assignment 8.9: PUT and DELETE

Add two more endpoints to your app:

`PUT /student/:id` should expect the request body to include student name or email. If both are missing, the endpoint should return a response with status 400 and an error message. The endpoint should update an existing student identified by the request parameter *id*. 

`DELETE /student/:id` should remove a student identified by the request parameter *id*.

Both endpoints should return 404 if there is no student matching the request parameter *id*. On success both endpoint should return an empty response with status code 204.
