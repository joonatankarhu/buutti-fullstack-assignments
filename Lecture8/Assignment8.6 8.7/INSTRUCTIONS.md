
## Assignment 8.6: 404 Not Found

Add a **middleware function** that sends a response with **status code** 404 and an appropriate error message, if user tries to use an endpoint that does not exist.

If you have not already done so, move all your middleware to a separate file, to keep your program clean and readable.


## Assignment 8.7: Body Logging

Enable body parsing in your application. Modify your logger middleware so that in addition to existing functionality, it also logs the request body if it exists.
