import express, { Request, Response } from 'express'
import { middlewareLogger, errorHandler } from './middleware'

const server = express()
server.use(express.json())

const list = {}

server.use(middlewareLogger)

server.get('/students', (_req: Request, res: Response) => {
  res.send(list)
})
server.use(errorHandler)

server.listen(3000, () => {
  console.log('listening to port 3000')
})
