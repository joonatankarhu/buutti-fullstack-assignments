import express, { Request, Response, NextFunction } from 'express'

export const middlewareLogger = (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  if (req.body) {
    console.log('Request body: ', req.body)
  }

  const time = new Date()
  const method = req.method
  const url = req.url
  console.log({ reqTime: time, reqMethod: method, reqUrl: url })

  next()
}

export const errorHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.status(404).send()
}