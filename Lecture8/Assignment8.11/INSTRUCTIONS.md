
## Assignment 8.11: Books API

Create an express REST-API, which offers CRUD for your books. It should include following endpoinst
- `GET /api/v1/books` → Returns a list of all the books
- `GET /api/v1/books/{id}` → Returns a book with a corresponding ID.
- `POST /api/v1/books` → Creates a new book.
- `PUT /api/v1/books/{id}` → Modifies an existing book
- `DELETE /api/v1/books/{id}` → Removes a book with a corresponding id

Every book should have four parameters
- id (number)
- name (string)
- author (string)
- read (boolean)

**Note**: You don’t need to enable an actual database at this point. Simple runtime-cache (For example: let books = []) will do for now. Do not save to a file, as we might end up clouding this application later

**Hint**: Remember to use *nodemon* as dev-dependency to aid you in the development.