import express, { Request, Response } from 'express'

const app = express()
app.use(express.json())

interface Book {
  id: number
  name: string
  author: string
  read: boolean
}

let books: Array<Book> = []

app.post('/api/v1/books', (req: Request, res: Response) => {
  const newBook: Book = req.body

  books.push(newBook)

  res.status(201).send('Created a book')
})

app.get('/api/v1/books', (_req: Request, res: Response) => {
  if (books.length < 1 || undefined) {
    res.status(404).send('Error: there are no books in the list...')
  }
  res.status(200).send(books)
})

app.put('/api/v1/books/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)

  if (books.length > 0) {
    const newBook = req.body

    const findBookById = books.filter((book) => book.id === id)
    const book = findBookById[0]

    const editBook = (book: any, newBook: any): void => {
      if (book !== newBook) {
        book.name === newBook.name
          ? null
          : (book = { ...book, name: newBook.name })
        book.author === newBook.author
          ? null
          : (book = { ...book, author: newBook.author })
        book.read === newBook.read
          ? null
          : (book = { ...book, read: newBook.read })

        const index = books.findIndex((book) => book.id === id)
        if (index !== -1) {
          books[index] = book
          res.status(201).send(books)
        }
      } else {
        res.status(400).send('No changes were made')
      }
    }

    editBook(book, newBook)
  }
})

app.get('/api/v1/books/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const targetBook = books.filter((book) => book.id === id)
  res.status(200).send(targetBook[0])
})

app.delete('/api/v1/books/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const index = books.findIndex((book) => book.id === id)
  if (index !== -1) {
    books = books.splice(index - 1, 1)
    console.log(books)
    res.status(200).send('Removed book')
  }
})

app.listen(3000, () => console.log('Listening on port 3000'))
