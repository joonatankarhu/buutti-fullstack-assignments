import express, { Request, Response } from 'express'

const server = express()

server.listen(3000, () => {
  console.log('listening to port 3000')
})

const counter = {
  count: 0,
}

server.get('/counter', (_req: Request, res: Response) => {
  counter.count++
  res.send(counter)
})
