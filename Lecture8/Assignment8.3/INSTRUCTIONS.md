
## Assignment 8.3: Counter Server

Create an API that consists of only one endpoint:  `/counter`

Whenever you enter this endpoint from the browser, it
should respond with a JSON object with information on how many times the endpoint has been accessed.
