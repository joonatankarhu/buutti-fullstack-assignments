import express, { Request, Response } from 'express'
import { middlewareLogger, errorHandler } from './middleware'

const server = express()
const port = 3000
server.use(express.json())
server.use(middlewareLogger)

server.get('/students', (_req: Request, res: Response) => {
  const studentId = studentList.map((student) => student.id)
  res.send(studentId)
})

const studentList: Array<any> = []
server.post('/student', (req: Request, res: Response) => {
  const { id, name, email } = req.body
  type Student = {
    id: number
    name: string
    email: string
  }
  const student: Student = {
    id,
    name,
    email,
  }
  studentList.push(student)
  console.log('studentList', studentList)
  res.status(201).send(studentList)
})
///////// JOONA! This wont work in insomnia, i used postman //////////////
server.get('/student/:id', (req: Request, res: Response) => {
  const id = Number(req.params.id)
  const data = studentList.find((stud) => stud.id === id)
  if (data === undefined) {
    res.status(404).send(studentList + 'Error: Student not found.')
  }
  res.send(data)
})

server.use(errorHandler)

server.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
