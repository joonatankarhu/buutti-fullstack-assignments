## Assignment 8.8: POST Requests

Add two more endpoints to your app:

`POST /student` should expect the request body to include student id, name and email. If some of these parameters are missing, the endpoint should return a response with status 400 and an error message. The endpoint should store the student information in an array called *students*. The endpoint should return an empty response with status code 201.

`GET /student/:id` should return the information of one single student, identified by the *id* request parameter. The response should be in JSON format. If there is no such student, the endpoint should return 404. 

Modify the `GET /students` endpoint to return the list of all student *ids*, without names or emails.