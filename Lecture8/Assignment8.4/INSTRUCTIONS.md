
## Assignment 8.4: Advanced Counter Server

Expand the API from the previous assignment by accepting a **name** through the counter endpoint: `/counter/:name`

When entering this endpoint, the server should return the count of how many times this named endpoint has been visited.

For example
 - Aaron enters /counter/Aaron 🠖 "Aaron was here 1 times"
 - Aaron enters /counter/Aaron 🠖 "Aaron was here 2 times"
 - Beatrice enters /counter/Beatrice 🠖 "Beatrice was here 1 times"
 - Aaron enters /counter/Aaron 🠖 "Aaron was here 3 times"
