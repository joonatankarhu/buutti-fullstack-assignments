import express, { Request, Response } from 'express'

const server = express()
const port = 3000

const counter = {
  count: 0,
  name: ' ',
}
const arr = [counter]
server.get('/counter/:name', (_req: Request, res: Response) => {
  for (const obj of arr) {
    if (obj.name === _req.params.name.toString()) {
      obj.count++
      res.send(obj)
    }
  }
  const ownCounter = { ...counter }
  ownCounter.count++
  ownCounter.name = _req.params.name.toString()
  arr.push(ownCounter)
  res.send(ownCounter)

  res.send(counter)
})
server.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
