## Assignment 3.9: FizzBuzz

Create a program that **loops through numbers from 1 to 100** and
- if the number is **divisible by 3**, prints "Fizz"
- if the number is **divisible by 5**, prints "Buzz"
- if the number is **divisible by both** (3 and 5), prints "FizzBuzz"
- if no previous conditions apply, prints just the number

