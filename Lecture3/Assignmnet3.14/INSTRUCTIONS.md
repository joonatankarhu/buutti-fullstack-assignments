## Assignment 3.14 ATM
Create a ATM program to check your balance. Create variables ``balance``, ``isActive``, ``checkBalance``. Write conditional statement that implements the flowchart below.

![](atm-flowchart.png "ATM flowchart")

Change the values of `balance`, `checkBalance`, and `isActive` to test your code! 