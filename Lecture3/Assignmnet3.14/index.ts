// to run the code in cmd, npx nodemon index.ts numValue

const checkBalance: boolean = true;
const isActive: boolean = true;
const balance: number = 200;

if (checkBalance) {
  if (isActive && balance > 0) {
    console.log(`Your balance is ${balance}`);
  } else if (!isActive) {
    console.log("Your account is not active.");
  } else {
    if (balance === 0) {
      console.log("Your account is empty.");
    } else {
      console.log("Your balance is negative");
    }
  }
} else {
  console.log("Have a nice day!");
}
