## Assignment 3.7: Sum of Ints

Write a program that prints the **sum of integers from 1 to n**, with a given number n. For example, if n = 5, program prints 15.

Solve this task with **both** a *for loop* and a *while loop*.