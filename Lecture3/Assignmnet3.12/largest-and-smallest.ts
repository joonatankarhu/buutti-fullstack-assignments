// to run the code in cmd, npx nodemon index.ts numValue numValue numValue
const num1 = parseInt(process.argv[2]);
const num2 = parseInt(process.argv[3]);
const num3 = parseInt(process.argv[4]);

const printNum = (num1: number, num2: number, num3: number) => {
  const highestNum = Math.max(num1, num2, num3);
  console.log(`Highest number is: ${highestNum}`);

  const lowestNum = Math.min(num1, num2, num3);
  console.log(`Lowest number is: ${lowestNum}`);

  const arr: number[] = [num1, num2, num3];
  arr.every((val: number) => val === arr[0])
    ? console.log(`All numbers are equal, ${num1}, ${num2}, ${num3}`)
    : null;
    // perhaps an if statement here instead of ternary, since the last one is just null?
};
printNum(num1, num2, num3);
