## Assignment 3.18: Replace characters (difficult)
Create a program that takes in a string, and replaces every occurrence of your given character with your other given character.

example: ``node .\replacecharacters.js g h "I have great grades for my grading"`` -> ``I have hreat hrades for my hrading``

* Hint: https://www.w3schools.com/jsref/jsref_replace.asp
