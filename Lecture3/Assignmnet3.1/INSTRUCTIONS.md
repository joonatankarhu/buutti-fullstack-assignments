## Assignment 3.1: JS NPM Project

Create a new **JavaScript** Node project using `npm init`. 

Install the [mathjs](https://www.npmjs.com/package/mathjs) dependency, and use it to print the value of **pi** rounded to ten decimal places.

You can check the mathjs NPM page for more information.