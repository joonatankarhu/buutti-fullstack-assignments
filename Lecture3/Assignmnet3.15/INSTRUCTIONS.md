## Assignment 3.15 Initial letters
Create a program that takes in 3 names and outputs only initial letters of those name separated with dot.

example: ``node .\initialLetters.js Jack Jake Mike`` -> ``j.j.m``
1.11 String length comparison
Create a program that takes in 3 names, and compares the length of those names. Print out the names ordered so that the longest name is first.

example: ``node .\lengthcomparison.js Maria Joe Philippa`` -> ``Philippa Maria Joe``