const name1 = process.argv[2]
const name2 = process.argv[3]
const name3 = process.argv[4]

const list = [name1, name2, name3]
// const firstChar = list.map((name) => name = name.charAt(0))
// const names = firstChar.join(".");
// console.log(names);

// String length comparison
const lengths = list.map((name) => name.length)
const lgthsAsNum = lengths.map((item) => (item = parseInt(item)))

const maxNum = Math.max(...lgthsAsNum)
const minNum = Math.min(...lgthsAsNum)

const maxString = list.filter((item) => item.length == maxNum)
const middleString = list.filter(
  (item) => item.length != maxNum && item.length != minNum
)
const minString = list.filter((item) => item.length == minNum)

const newList = []
newList.push(maxString[0])
newList.push(middleString[0])
newList.push(minString[0])

console.log(newList)
