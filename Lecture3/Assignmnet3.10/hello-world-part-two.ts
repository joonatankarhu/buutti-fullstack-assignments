const input = process.argv[2];

const printHiInLanguages = () => {
  if (input === "fi") {
    console.log("Hei maailma!");
  } else if (input === "es") {
    console.log("Hola Mundo!");
  } else if (input === "sv") {
    console.log("Hej världen!");
  } else {
    console.log("Hello world!");
  }
};
printHiInLanguages();
// is the function here necessary?
