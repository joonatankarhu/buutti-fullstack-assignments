## Assignment 3.10: Hello World, part 2
Create a program that takes in one argument from command line, a language code (e.g. "fi", "es", "en"). Console.log "Hello World" for the given language for atleast three languages. It should default to console.log "Hello World". 

Remember to test that the program outputs the right answer in all cases.
* Hint: use process.argv for input