const arr: Array<string> = [
  "banaani",
  "omena",
  "mandariini",
  "appelsiini",
  "kurkku",
  "tomaatti",
  "peruna",
];

console.log(arr[2], arr[4], arr.length);
console.log(arr.sort());
arr.push("sipuli");
console.log(arr);

// extra
console.log(arr.shift());
arr.forEach((item: string) => {
  console.log(item);
});

const arrayFilteredWithR = () => {
  arr.map((item: string): void => {
    if (item.includes('r')) {
      console.log('items with r: ', item);
    }
  });
};
arrayFilteredWithR();