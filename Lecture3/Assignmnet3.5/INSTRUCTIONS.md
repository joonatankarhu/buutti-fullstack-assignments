```
const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"]
```

1. Print the 3rd and 5th items of the array and the array’s length
2. Then sort the array in alphabetical order and print the entire array
3. Finally, add the item “sipuli” to the array, and print out again

**EXTRA:**

4. Remove the first item in the array, and print out again. HINT: shift()
5. Print out every item in this array using .forEach()
6. Print out every item that contains the letter ‘r’. HINT: includes()