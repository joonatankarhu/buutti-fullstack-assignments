## Assignment 3.2: Hello TS

Create a Hello World program using **TypeScript**.

1. If you have not yet done so, install typescript globally using NPM.
2. Create a new folder for your project
3. Add a hello.ts file, and write a program that prints “Hello TypeScript!”.
4. Compile your TS program into JS.
5. Run the compiled program using Node.