## Assignment 3.6: Using the For Loop

Using the for loop for each problem, print out the following number sequences:

1. 0 100 200 300 400 500 600 700 800 900 1000
2. 1 2 4 8 16 32 64 128
3. 3 6 9 12 15
4. 9 8 7 6 5 4 3 2 1 0
5. 1 1 1 2 2 2 3 3 3 4 4 4
6. 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4
