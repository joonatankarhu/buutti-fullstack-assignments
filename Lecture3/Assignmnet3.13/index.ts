// to run the code in cmd, npx nodemon index.ts numValue
const month = parseInt(process.argv[2]);

function daysInMonth (month: number) {
  const daysInMonth = new Date(2023, month, 0).getDate();
  // I see you're no stranger to the Date object :)
  console.log(`There are ${daysInMonth} days`);
}
daysInMonth(month);