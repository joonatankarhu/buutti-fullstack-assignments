## Assignment 3.3: TS NPM Project

Following lecture instructions, create a TypeScript NPM project.

Your program should print a Hello TypeScript message, like in Assignment 3.1.

The user should be able to compile and run the project using NPM scripts.