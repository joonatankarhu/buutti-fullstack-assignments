## Assignment 3.8: Sum of Some Ints

Modify the previous program so that only the multiples of **three and five** are considered in the sum. For example: if n=17, we’d accept 3, 5, 6, 9, 10, 12 and 15, and the sum would be 60.

**Hint**: The modulus operator will help you with this.