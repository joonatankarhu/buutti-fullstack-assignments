// npm install --save-dev nodemon ts-node

const printNum = (n: number) => {
  let sum = 0;
  // much better variable name!
  for (let i = 0; i <= n; i++) {
    if(i % 3 === 0|| i % 5 === 0) {
      sum = sum + i;
    }
  }
  console.log(sum);
}
printNum(17);