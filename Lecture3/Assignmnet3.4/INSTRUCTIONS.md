## Assignment 3.4: ESLint

Create an empty project and install **ESLint** there. Add the following rule to eslint.json: `"no-var": "error"`
Add this code to the folder and use ESLint to fix it!
```
var foo = 1
console.log(foo)
const bar
bar = 1
function test(


    ) {
  console.log(baz)
}
var baz = 123
```