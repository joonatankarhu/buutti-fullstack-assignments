const caseOption = process.argv[2]
const message = process.argv[3]

if (caseOption === 'lower') {
  const filteredMessage = message.toLowerCase()
  console.log(filteredMessage)
} else if (caseOption === 'upper') {
  const filteredMessage = message.toUpperCase()
  console.log(filteredMessage)
}
