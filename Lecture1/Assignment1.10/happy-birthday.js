// ## Assignment 1.10: Happy Birthday

const lastName = 'Doe'
const age = 65
const newAge = age + 1
const isDoctor = true
const sender = 'Joonatan Karhu'

let title
let nth

const ageString = newAge.toString().slice(-1)

switch (ageString) {
  case 1:
    nth = 'st'
    break
  case 2:
    nth = 'nd'
    break
  case 3:
    nth = 'rd'
    break
  default:
    nth = 'th'
    break
}

const NEXT_AGE = `${newAge}${nth}`

if (isDoctor) {
  title = 'Dr.'
} else {
  title = 'Mx.'
}

console.log(`
Dear ${title} ${lastName}

Congratulations on your ${NEXT_AGE} birthday! Many happy returns!

Sincerely,
${sender}`)
