
// const people = process.argv[2];
// const size = process.argv[3];
// const evenGroups = people / size;
// const groups = (people - (people % size)) / size + 1;

// people % size === 0
//   ? console.log(`Number of groups: ${evenGroups}`)
//   : console.log(`Number of groups: ${groups}`);

// // Extra assignment:
const people = process.argv[2];
const size = process.argv[3];

const groups = (people + size - 1) / size - (people + size - 1) % size / size;

console.log(`Number of groups: ${groups}`);