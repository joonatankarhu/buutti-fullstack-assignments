## Assignment 1.1: Starting Out

- Create a working directory for your JavaScript exercises
- Create a Hello world script


**Important**: name your files well! (instead of asdasd.js, use something like exercise1.1.js) 

Open **command prompt** and go to the working directory (you can also use VSC’s terminal window for this)

**Run** the script!