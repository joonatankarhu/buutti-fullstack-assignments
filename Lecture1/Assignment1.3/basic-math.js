const calculate = (a, b) => {
  const sum = a + b
  const difference = a - b
  const fraction = a / b
  const product = a * b

  // extra 1
  const expo = a ** b
  const modulo = a % b

  // extra 2
  const avrg = (a + b) / 2

  console.log('sum: ', sum)
  console.log('difference: ', difference)
  console.log('fraction: ', fraction)
  console.log('product: ', product)

  // extra 1
  console.log('expo: ', expo)
  console.log('modulo: ', modulo)
  
  // extra 2
  console.log('avrg: ', avrg)
}

calculate(5, 10)
