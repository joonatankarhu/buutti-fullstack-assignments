## Assignment 1.3: Basic Math

Create a program that takes in two numbers, **a** and **b** (decide the value for these freely).
1. Calculate the *sum*, *difference*, *fraction* and *product* of these numbers.
2. console.log the results.

**EXTRA 1**: Calculate the *exponentiation* and *modulo* of **a** and **b**.

**EXTRA 2**: Calculate and log the *average* of the two numbers, **a** and **b**.