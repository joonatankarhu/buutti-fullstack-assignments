const distance = 10 // kilometers
const speed = 20 // km / h

const travelTime = (distance / speed) * 60
console.log(`Travel time is ${travelTime} minutes`)
