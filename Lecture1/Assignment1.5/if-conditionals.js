const playerCount = 4
playerCount === 4
  ? console.log('Happy playing with Game hearts!')
  : console.log('Not enough players or too many.')

const markIsStressed = false
const markHasIcecream = true
!markIsStressed && markHasIcecream
  ? console.log('Mark is Happy')
  : 'Mark is sad...'

const isSunShinning = true
const isRaining = true
const temperature = 20
isSunShinning && !isRaining && temperature >= 20
  ? console.log('It is a beach day!')
  : console.log('it is not a beach day...')

// d
const seesSuzy = true
const seesDan = false
;(seesSuzy && seesDan) || (!seesSuzy && !seesDan)
  ? console.log('Arin is sad...')
  : console.log('Arin is happy!')
