## Assignment 1.5: If Conditionals

By using a *single* **if conditional**, write the following conditions:
- The game hearts can only be played with 4 people (playerCount)
- Mark is happy when he is not stressed or if he has ice cream (isStressed, hasIcecream)
- It is a beach day if the sun is shining, it is not raining and the temperature is 20 degrees Celsius or above
- Arin is happy if he sees either Suzy or Dan on Tuesday night. However, seeing them both at the same time, or being alone, makes him sad.