const myString = 'This is a string'
const myNumber = 1
const myBoolean = true

console.log('value:', myString, 'type:', typeof myString)
console.log('value:', myNumber, 'type:', typeof myNumber)
console.log('value:', myString, 'type:', typeof myString)

myNumber = 'blue'

console.log('value:', myString, 'type:', typeof myString)
console.log('value:', myNumber, 'type:', typeof myNumber)
console.log('value:', myString, 'type:', typeof myString)
