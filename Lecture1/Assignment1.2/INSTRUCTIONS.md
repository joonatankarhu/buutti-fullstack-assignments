## Assignment 1.2: Primitives

Write a program that declares three variables. One of the variables should be a *string*, one a *number*, and one a *boolean*.

Log each of these variables, and their data type using the `typeof` method.

Change one of the variables to include a new value. Log the values and types of the variables again. 

Include at least one comment in your program.