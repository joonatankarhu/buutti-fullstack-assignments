Create a program that takes in a **number from command line** that represents a length of a squares sides. Calculate the **area** of the square with given number.

A square with sides of length 5m is 25 square meters in area. So in this assignment if you would run the program with 5 as the parameter `node area_of_a_square.js 5`, the program would print `25`.

**Hint**: Command line arguments are found in an array called:

``process.argv``

```javascript
const a = process.argv[2];
const b = process.argv[3];

console.log(a); // food
console.log(b); // beer
```

``node filename.js food beer``
