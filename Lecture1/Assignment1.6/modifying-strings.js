const validateString = () => {
  const input = process.argv[2]

  // regular expressions
  const hasWhiteSpace = /^\s+|\s+$/g
  const hasUpperCase = /^[A-Z]/

  if (
    typeof input === 'string' &&
    !hasWhiteSpace.test(input) &&
    input.length < 20 &&
    !hasUpperCase.test(input)
  ) {
    console.log(input)
  } else {
    console.log('Error, Invalid input...')
  }
}

validateString()
