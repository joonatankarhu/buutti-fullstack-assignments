## Assignment 1.4: Working with Strings

Write a program that has 2 strings, **str1** and **str2**
1. Create a *combined string*, **str_sum** by using the + operator or using template string, and print the result.
2. Print the *lengths* of these strings using the "length" property.
3. Calculate and print **avgLength**, the *average length* of the two strings
4. Print first string converted to *uppercase*
5. Print the *fist and last letter* of the second string