## Assignment 1.9: Seconds in a Year

Calculate how many seconds there are in a year. Use variables for **days, hours, minutes and seconds in a year**. Print out the result with console.log.