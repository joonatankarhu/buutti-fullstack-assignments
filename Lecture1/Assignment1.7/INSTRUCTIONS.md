## Assignment 1.7: Discounted Price

Create variables for **price**, for price in euros, and **discount**, for a discount percentage, and assign some values for those. Calculate and console.log the *original price*, *discount percentage*, and the *discounted price*.