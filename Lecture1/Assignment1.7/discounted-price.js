// My comment
const price = 20
const discount = 50

const originalPrice = price
const discountPercentage = discount

const discountedPrice = ((100 - discountPercentage) / 100) * price

console.log(`originalPrice: ${originalPrice}€`)
console.log(`discountPercentage: ${discountPercentage}%`)
console.log(`discountedPrice: ${discountedPrice}€`)
