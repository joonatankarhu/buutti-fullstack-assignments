import Form from "./components/Form"

function App() {

  return (
    <>
      <h1>Feedback!</h1>
      <Form/>
    </>
  )
}

export default App
