import { useEffect, useState } from 'react'
import './Form.css'

const Form = () => {
  const [type, setType] = useState('')
  const [textarea, setTextarea] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')

  const [validated, setValidated] = useState(true)

  const handleTypeChange = (e: any) => {
    const type = e.target.value
    setType(type)
  }

  const handleTextareaChange = (e: any) => {
    const textarea = e.target.value
    setTextarea(textarea)
  }

  const handleNameChange = (e: any) => {
    const name = e.target.value
    setName(name)
  }
  
  const handleEmailChange = (e: any) => {
    const email = e.target.value
    setEmail(email)
  }

  const clearForm = () => {
    setType('')
    setTextarea('')
    setName('')
    setEmail('')
  }

  useEffect(() => {
    if (type !== '' && textarea !== '') {
      setValidated(false)
    }
  }, [type, textarea])
  

  const handleSubmit = (e) => {
    e.preventDefault()

    const formData = {
      type,
      textarea,
      name,
      email
    }
    console.log(formData)
    clearForm()
  }

  return (
    <form onSubmit={handleSubmit} className='container'>
      <div className='radio-btns'>
        <input 
          type="radio" 
          name="feedback" 
          value="feedback" 
          checked={type === 'feedback'}
          onChange={handleTypeChange}
        />
        <label htmlFor="regular">Feedback</label>

        <input 
          type="radio" 
          name="suggestion" 
          value="suggestion" 
          checked={type === 'suggestion'}
          onChange={handleTypeChange}
        />
        <label htmlFor="medium">Suggestion</label>

        <input 
          type="radio" 
          name="question" 
          value="question" 
          checked={type === 'question'}
          onChange={handleTypeChange}
        />
        <label htmlFor="large">Question</label>
      </div>

      <div className='message'>
        <p>Message</p>
        <textarea 
          rows={3}
          value={textarea}
          onChange={handleTextareaChange}
        >
        </textarea>
      </div>

      <div className='user-info'>
        <input 
          type="text" 
          value={name}
          onChange={handleNameChange}
          placeholder='name'
        />
        <input 
          type="email" 
          value={email}
          onChange={handleEmailChange}
          placeholder='email'
        />
      </div>

      <div className='action-btns'>
        <button type='submit' disabled={validated}>
          Send
        </button>
        <button onClick={clearForm}>
          Clear form
        </button>
      </div>
    </form>
  )
}

export default Form