Using [API Ninjas "Dad Jokes" API](https://api-ninjas.com/api/dadjokes), create a React app that displays a new dad joke every time the page is refreshed.

API requires a key to be sent in the headers. You can use the key in the example code.

```javascript
const url = 'https://api.api-ninjas.com/v1/dadjokes'
const config = {
  headers: {
    'X-Api-Key': 'yourkey'
  }
}
const response = await axios.get(url, config)
```