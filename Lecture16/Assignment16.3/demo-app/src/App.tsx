import { useState, useEffect } from 'react'

function App() {
  const [joke, setJoke] = useState('')

  useEffect(() => {
    const getJoke = async () => {
      const url = 'https://api.api-ninjas.com/v1/jokes?limit=1'

      const response = await fetch(url, {
        method: "GET",
        headers: {
          'X-Api-Key': 'yourkey'
        }
      })
      
      const data = await response.json()
      setJoke(data[0].joke)
    }
    getJoke()
  }, [])

  return (
    <>
      <h1>Dad Jokes</h1>
      <p>{joke}</p>
    </>
  )
}

export default App
