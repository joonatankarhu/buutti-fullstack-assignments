import './BingoBall.css'

interface Props {
  number: number,
}

const BingoBall = ({ number }: Props) => {
  return (
    <div className='bingo-ball'>
      {number}
    </div>
  )
}

export default BingoBall