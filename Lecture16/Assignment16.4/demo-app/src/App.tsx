import './App.css'
import { useState } from "react"
import BingoBall from "./components/BingoBall"

function App() {
  const [balls, setBalls] = useState<number[]>([])

  const createBall = () => {
    const newBall = Math.floor(Math.random() * 40)
    const findBall = balls.find((ball) => ball === newBall)
    if(findBall) {
      return
    }

    setBalls(() => [...balls, newBall])
  }

  const reset = () => {
    setBalls(() => [])
  }

  return (
    <div className='container'>
      <div className='bingo-balls-container'>
        {balls && balls.map((number, i) => (
          <BingoBall number={number} key={i} />
        ))}
      </div>
      <div className='btns-container'>
        <div onClick={createBall} className='add-btn'>+</div>
        <div onClick={reset} className='reset-btn'>Reset</div>
      </div>
    </div>
  )
}

export default App
