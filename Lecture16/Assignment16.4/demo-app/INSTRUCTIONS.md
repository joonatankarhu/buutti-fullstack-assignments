## Assignment 16.4: Bingo

Your grandma wants to practice her bingo game to top level and wants you to build an app for practice
Program a React application with a button that adds a new random bingo number to the screen every time the button is pressed.

**Hint**: The bingo numbers should be stored in an array that is the value of the state.
**Extra**: Make it impossible to get the same number twice, Make your bingo balls pretty and create a reset button.