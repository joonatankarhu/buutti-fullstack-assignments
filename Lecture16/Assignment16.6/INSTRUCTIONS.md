## Assignment 16.6: More Cats

Create a Cat App using https://cataas.com/ API. The app should have three elements: a cat pic, text input, and a reload button. 

The app should open with a random cat pic and an empty input. If the reload is pressed when the input is empty, it should load another random cat pic.

If there is text in the input and the reload is pressed, it should load a new random cat picture with the text rendered into the picture.