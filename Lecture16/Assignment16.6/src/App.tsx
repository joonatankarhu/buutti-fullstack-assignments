import { useState } from "react"
import axios from 'axios'
import './App.css'

const url = 'https://cataas.com/cat'

function App() {
  const [image, setImage] = useState('')
  const [text, setText] = useState('')
  
  const textUrl = `https://cataas.com/cat/says/${text}`

  const getRandomCatImage = async () => {
    const catResponse = await axios.get("https://cataas.com/cat");
    console.log(catResponse);
    
     return catResponse.request.res.responseUrl
  
  }

  const handleChange = (e: any) => {
    setText(e.target.value)
  }
  
  const handleClick = async () => {
    if (text === '') {
      const newImage = await getRandomCatImage()
      // setImage(newImage)
      console.log(newImage);
      
    } else {
      const newImage = axios.get(textUrl)
      setImage(newImage)
    }
  }

  return (
    <>
      <div className="image">
        <img src={url} alt="Image of a cat" />
      </div>
      <div className="inputs">
        <input 
          type="text" 
          value={text} 
          onChange={handleChange}
        />
        <button onClick={handleClick}>
          Reload
        </button>
      </div>
    </>
  )
}

export default App
