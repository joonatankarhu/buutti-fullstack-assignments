## Assignment 16.7: Contact List

Create a contact list app. The app should have a two column layout. The left column should have a list of all the contacts. 

There should be an "Add contact" button in the left column. When user clicks that, a view to add new contact is shown in the right column. The view should have fields name, email, phone, address, website, and info. In addition there should be buttons save and cancel. If user clicks cancel, the app returns to the starting state. If the user clicks save, the contat is added to the list of contacts.

If user clicks one of the contacts from the left column, contact details are shown in the right column. Only saved information is shown, no empty fields should be visible. There should also be buttons edit and remove. If the user clicks remove, the contact is removed and the app returns to the starting state. If the user clicks edit, a contact editor opens in the right column. 

There should be a search box in the left column. If the user types a partial name to the search box, the list of contacts is reduced to include only the contacts whose name include the search term.