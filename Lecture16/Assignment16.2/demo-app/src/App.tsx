import { useState, useEffect } from 'react'

function App() {
  const [count, setCount] = useState(0)

  useEffect(() => {
    setTimeout(() => {
      setCount(count + 1)
    }, 1000)
  }, [count])

  return (
    <>
      <h1>Timer</h1>
      <p>{count}</p>
    </>
  )
}

export default App
