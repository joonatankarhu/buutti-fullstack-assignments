## Assignment 16.2: Timer

Using `useState()`, `useEffect()` and `setTimeout()`, create a timer that updates every second.
**Bonus 1**: Add a button that resets the timer to 0
**Bonus 2**: Add minutes to the timer and update them every 60 seconds
**Bonus 3**: Add buttons to add 1 minute or 1 second to the timer and also a button to start/stop it