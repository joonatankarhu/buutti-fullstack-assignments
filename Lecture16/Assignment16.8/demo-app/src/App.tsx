import './App.css'
import { useState } from 'react'
import Wrapper from './components/Wrapper'
import Screen from './components/Screen'
import ButtonBox from './components/ButtonBox'
import Button from './components/Button'

const btnValues = [
  ['%', '/', 'C', 'back'],
  [7, 8, 9, 'x'],
  [4, 5, 6, '-'],
  [1, 2, 3, '+'],
  [0, '.', '='],
]

function App() {
  const [num, setNum] = useState(null)
  const [sign, setSign] = useState('')
  const [calculatedVal, setCalculatedVal] = useState(0)

  const handleClick = (btn: any) => {
    setNum(btn)
  }

  return (
    <Wrapper>
      <Screen value={num ? num : calculatedVal} />
      <ButtonBox>
        {btnValues.flat().map((btn, i: number) => {
          return (
            <Button
              key={i}
              className={btn === '=' ? 'equals' : ''}
              value={btn}
              onClick={() => handleClick(btn)}
            />
          )
        })}
      </ButtonBox>
    </Wrapper>
  )
}

export default App
