// import backspace from '../assets/backspace.svg'
import './ButtonBox.css'

const ButtonBox = ({ children }: any) => {
  return <div className="buttonBox">{children}</div>
}

export default ButtonBox
