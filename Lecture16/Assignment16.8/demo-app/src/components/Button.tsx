import './Button.css'

interface Props {
  className: string,
  value: string | number,
  onClick: any
}

const Button = ({ className, value, onClick }: Props) => {
  return (
    <button className={className} onClick={onClick}>
      {value}
    </button>
  )
}

export default Button
