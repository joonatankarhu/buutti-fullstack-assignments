interface Props {
  value: number
}

const Screen = ({ value }: Props) => {
  return (
    <div>
      <input type="text" className="screen" value={value} disabled />
    </div>
  )
}

export default Screen
