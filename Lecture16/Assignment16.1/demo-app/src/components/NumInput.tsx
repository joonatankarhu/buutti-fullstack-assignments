import { ChangeEvent } from "react"

interface Props {
  inputNum: string,
  handleChange: (e: ChangeEvent<HTMLInputElement>) => void
}

const NumInput = ({ inputNum, handleChange }: Props) => {

  return (
    <input
      type="number"
      pattern="[0-9]*"
      value={inputNum}
      onChange={handleChange}
    />
  )
}

export default NumInput