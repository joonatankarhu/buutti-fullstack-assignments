import './app.css'
import { useState, ChangeEvent } from "react"
import NumInput from "./components/NumInput"

function App() {
  const [numberList, setNumberList] = useState<string[]>([])
  const [inputNum, setInputNum] = useState<string>('')

  const addNumber = () => {
    setNumberList([inputNum, ...numberList])
    setInputNum('')
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {

    setInputNum(e.target.value)

    if (inputNum.length >= 10) {
      addNumber()
    }
  }

  return (
    <>
      <h1>Phone numbers</h1>
      <NumInput inputNum={inputNum} handleChange={handleChange} />
      <br/>
      {numberList && numberList.map((item, i) => (
        <p key={i} style={{fontSize: '1.2em'}}>
          {item}
        </p>
      ))}
    </>
  )
}

export default App
