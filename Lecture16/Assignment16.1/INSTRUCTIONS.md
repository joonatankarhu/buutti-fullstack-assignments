## Assignment 16.1: Phone Numbers

Create a program that has a list of phone numbers. We boldly assume that a phone number consists of ten digits and has no other requirements. When the program starts, the list is empty. Add a function for adding a phone number to the list. 

Create a component that has an input field. The input field should take a numeric input of ten numbers. No any other character types allowed. When the input field has the required ten numbers, it calls the higher level function that adds the number to the phone number list. Then it empties the input field.