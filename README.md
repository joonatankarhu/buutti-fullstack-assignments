# Buutti Fullstack Program Assignments

Here are my assignments that I completed during my Full stack program. Because there are a lot of assignments, I created a list down below with some quick references to more meaningful assignments.

The first number is Lecture number and second number is the assignment.

# Diffculty levels
I rate the difficulty based on my own personal learning experience.
1 = easy
5 = difficult

## 1/5
 Assignment 3.9: FizzBuzz
 Assignment 3.15 Initial letters
 Assignment 4.2: Random Number
 Assignment 4.3: Function Flavours
 Assignment 4.4: Dices
 Assignment 4.5: Callback
 Assignment 4.6: Factorial
 Assignment 4.7: forEach
 Assignment 4.8: map
 Assignment 4.9: filter
 Assignment 4.11: reduce
 Assignment 4.12: Array Manipulation
 Assignment 4.13: Count Sheep
 Assignment 4.14: Indexed Alphabet
 Assignment 4.18: Palindrome
 Assignment 5.6: Modules
 Assignment 5.7: Read/Write File
 Assignment 5.8: Forecast
 Assignment 5.9: Simple Calculator
 Assignment 6.3: Random Async
 Assignment 6.5: Movie Search
 Assignment 6.8: Todos
 Assignment 8.1: Simple Request & Response
 Assignment 8.2: Simple Express Server
 Assignment 8.4: Advanced Counter Server
 Assignment 8.5: Logger Middleware
 Assignment 9.2: Students Router
 Assignment 15.2: Create a React App
 Assignment 15.3: Create a Components
 Assignment 15.4: Component with Props


## 2/5
 Assignment 4.15: Ordinal Numbers
 Assignment 4.17: Reversed Words
 Assignment 4.19: Array Randomizer
 Assignment 4.21: Prime Numbers
 Assignment 5.10: Likes
 Assignment 5.11: Above Average
 Assignment 5.13: Check the Exam
 Assignment 6.6: Largest Number 
 Assignment 6.7: Student Grades
 Assignment 8.8: POST Requests
 Assignment 9.5: Environmental Login
 Assignment 9.6: Create a Token# Assignment 9.7: Verify a Token
 Assignment 9.9: Test with Supertest
 Assignment 13.1: Database Testing
 Assignment 13.4: Express + PG + Azure
 Assignment 13.5: Pipeline with PG
 Assignment 15.6: Counters
 Assignment 16.2: Timer
 Assignment 15.7: User Input
 Assignment 16.1: Phone Numbers


## 3/5
 Assignment 4.20: Fibonacci Sequence
 Assignment 5.2: Objective Recipes
 Assignment 5.14: Credential Generator
 Assignment 5.15: Collatz Conjecture
 Assignment 7.1: Azure Function Project
 Assignment 7.7: Command Line Converter
 Assignment 8.9: PUT and DELETE
 Assignment 8.10: Score Checker API
 Assignment 9.4: Login
 Assignment 10.9: Notice Board
 Assignment 16.4: Bingo



## 4/5
 Assignment 5.3: Prototype Recipes
 Assignment 5.4: Classy Recipes
 Assignment 5.5: Extended Recipes
 Assignment 6.9: Race Cars
 Assignment 8.11: Books API
 Assignment 9.3: Registration
 Assignment 9.8: Securing a Route
 Assignment 10.5: Dockerized Node App
 Assignment 10.6: Multi Stage Build
 Assignment 10.8: Whale Read Fibonacci
 Assignment 12.6: Node & PostgreSQL
 Assignment 15.8: Todo application
 Assignment 16.5: Feedback



## 5/5

 Assignment 9.10: Securing Books API
 Assignment 9.11: Two Levels of Users
 Assignmnet 10.10: Secure Notice Board
 Assignment 11.6: Calendar API
 Assignment 11.9: Deploy Calendar API via CI/CD
 Assignment 12.13: Forum API


## Extreme:
 Assignment 16.7: Contact List

 Assignments for Lecture 14 Creating a banking API with Express and PostgreSQL. (group work)



