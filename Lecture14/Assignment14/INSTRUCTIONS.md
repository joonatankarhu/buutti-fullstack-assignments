# Assignments for Lecture 14

In this group assignment you are creating a banking API with Express and PostgreSQL.

The finished product must
    - Be deployed to Azure using a Docker container
    - Be deployed usining Gitlab CI/CD pipeline
    - Utilize a PostgreSQL database hosted in Azure
    - Include tests and linter

Users need to register to the service before they can use it. Users are authenticated by username and password. When user logs in with correct credentials, they get a token. The token must be present in all requests except register and login requests. Users are able to update their usernames and passwords. Users can also delete their user accounts. Deleting a user account also deletes all bank accounts associated with it.

Users have bank accounts that have money in them. User can have multiple bank accounts. When user registers to the service, the first bank account is automatically created for them. The initial balance in the bank account is 0. The user can create more bank accounts, up to total of three. Users can also delete individual bank accounts.

Users are able to list all the bank accounts associated with their user account. They can add or remove money from accounts, or transfer money from their account to another (either their or somebody else's) bank account. The balance of an account can not be negative at any point. They are also able to check the balance of any of their accounts.

Users are also able to request funds from another account by making a fund request. They are essentially asking for some specified amount of money to be transferred from another account to their account. If the owner of the account accepts the request, the funds are transferred. If the owner rejects the request, nothing happens.

These features must be implemented with following endpoints:

1. Register a new user
Endpoint: POST /users/register
Parameters: username, password
Return value: JSON Webtoken

2. Log in an existing user
Endpoint: POST /users/login
Parameters: username, password
Return value: JSON Webtoken

3. Update password
Endpoint: PATCH /users/password
Parameters: password
Return value:

4. Update username
Enpoint: PATCH /users/username
Parameters: username
Return value: 

4. Delete an existing user
Endpoint DELETE /users
Parameters: -
Return value: -

5. Create a new account
Endpoint: POST /accounts
Parameters: initial balance
Return value: account id, balance

6. Close an existing account
Endpoint: DELETE /accounts/:accountId
Parameters: -
Return value: -

7. List all user's accounts
Endpoint: GET /accounts
Parameters: -
Return value: list of account id's

8. Check account balance
Endpoint: GET /accounts/:accountId
Parameters: -
Return value: balance

8. Withdraw money
Endpoint: PATCH /accounts/:accountId/withdraw
Parameters: amount to withdraw
Return value: new balance

9. Deposit money
Endpoint: PATCH /accounts/:accountId/deposit
Parameters: amount to deposit
Return value: new balance

10. Transfer money
Endpoint: PATCH /accounts/transfer
Parameters: source account id, target account id, amount to transfer
Return value: new balance of the source account

Feature: Fund Requests
The implementation of this feature is left to your group to decide. The idea is that a user can make a funds request to other users. The recipient of the request can reject the request, in which case nothing happens. If they accept the request, the requested amount is transferred from their account to the account specified in the request.


