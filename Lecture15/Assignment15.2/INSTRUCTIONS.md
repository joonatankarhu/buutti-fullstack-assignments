## Assignment 15.2: Create a React App

Create a new React project with the command
```
npm init vite@latest
```
Install the dependencies and then run it with 
```
npm run dev
```
See that the project was created properly (http://127.0.0.1:5173/).
- Take a quick look at the source code
- Install React Dev Tools (if you have not done so already)
- Take a look at your browser’s Developer Tools 
and React Dev Tools