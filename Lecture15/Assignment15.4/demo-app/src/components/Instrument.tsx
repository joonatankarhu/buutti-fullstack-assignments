import './instrument.css'

interface Props {
  name: string,
  image: string,
  price: number
}

const Instrument = (props: Props) => {
  const { name, image, price } = props
  return (
    <div className="instrument">
      <div className='image-container'>
        <img src={image} />
      </div>
      <div className='details'>
        <p>{name}</p>
        <p className='price'>€{price}</p>
      </div>
    </div>
  )
}

export default Instrument