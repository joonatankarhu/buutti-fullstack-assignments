import './app.css'
import Instrument from './components/Instrument'
import guitar from './assets/guitar.png'
import violin from './assets/violin.png'
import tuba from './assets/tuba.png'


function App() {

  return (
    <>
      <div className='header'>
        <h1>Instruments</h1>
      </div>
      <div className='container'>
        <Instrument
          name="Guitar"
          image={guitar}
          price={500}
        />
        <Instrument
          name="Violin"
          image={violin}
          price={700}
        />
        <Instrument
          name="Tuba"
          image={tuba}
          price={600}
         />
      </div>
    </>
  )
}

export default App
