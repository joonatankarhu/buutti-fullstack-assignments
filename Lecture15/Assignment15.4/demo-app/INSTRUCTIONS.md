## Assignment 15.4: Component with Props

Create again a new React app and remove the parts you don’t need.

Create a component *Instrument*. It should include an instrument name, image, and price. You can display them as you wish, but *don’t add any styles* until you have the basic functionality working.

Your app should display a heading, and three instruments, each with an image and a price. You can use the [guitar](guitar.png), [violin](violin.png), and [tuba](tuba.png).

**Extra**: Add some css and make it look nice.
