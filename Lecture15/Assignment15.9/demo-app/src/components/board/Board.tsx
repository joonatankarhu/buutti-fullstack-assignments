import './board.css'
import Option from './option/option'

interface Props {
  boardOptions: number[]
}

const Board = ({ boardOptions }: Props) => {
  return (
    <div className='board'>
      {boardOptions.map((option: number) => {
        return (
          <Option id={option} key={option} />
        )
      })}
    </div>
  )
}

export default Board