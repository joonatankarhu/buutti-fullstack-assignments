import { useState } from 'react'
import './option.css'

interface Props {
  id: number
}

const players = ['x', 'y']
let index = 0

const boardOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9]

const Option = ({ id }: Props) => {
  const [boxArg, setBoxArg] = useState('')
  const [currentPlayer, setNextPlayer] = useState(players[index])

  
  const handleClick = (e: any) => {
    // const targetId = parseInt(e.target.id)
    if (index === 1) {
      
      setNextPlayer(players[index])
      
      setBoxArg(currentPlayer)
      index = 0
      return
    }

    setNextPlayer(players[index])
    setBoxArg(currentPlayer)
    index = index + 1
    return
  }

  return (
    <div 
      id={`${id}`} 
      onClick={handleClick}
      className="option-box"
    >
      {boxArg}
    </div>
  )
}

export default Option