import Board from "./components/board/Board"
import './app.css'

function App() {

  const boardOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  
  return (
    <>
      <h1>Tic Tac Toe!</h1>
      <Board boardOptions={boardOptions} />
    </>
  )
}

export default App
