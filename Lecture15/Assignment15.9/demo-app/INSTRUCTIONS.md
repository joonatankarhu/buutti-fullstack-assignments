
## Assignment 15.9 - Tic Tac Toe

Create a Tic Tac Toe game in React. The game should have 3x3 board, and when the user clicks a board square the appropriate symbol (X or O) should appear. 
 - The Player should change automatically, so in consecutive clicks, first one marks X, the second one O, the third again X and so on.
 - Current player should be visible
 - Players should not be able to select already selected squares
 - If all the squares are taken, the game should inform the players that the game is over
 - The game should have a reset button that restarts the game

**Extra**: Make the game keep track if one player has won. When one of the players gets a streak of three, either vertically, horzontally, or diagonally, the game should end. At this point game informs players which player has won. No more moves can be made at this point.