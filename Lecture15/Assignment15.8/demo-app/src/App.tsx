import { ChangeEvent, useState } from 'react'
import Todos from './components/Todos'
import './app.css'

interface Todo {
  id: number,
  todo: string
}

function App() {
  const [todo, setTodo] = useState('')
  const [todos, setTodos] = useState<Todo[]>([])
  const [showTodos, setShowTodos] = useState(false)
  const [newId, setNewId] = useState(0)
  

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setTodo(e.target.value)
  }

  const handleAddTodo = () => {
    const newTodo: Todo = { 
      id: newId,
      todo: todo 
    }
    setNewId(newId + 1)
    setTodos((prevTodos) => [...prevTodos, newTodo])
    setShowTodos(true)
  }
  

  const handleClick = () => {
    setShowTodos(!showTodos)
  }

  const handleDelete = (todoToDelete: Todo) => {
    setTodos(() => todos.filter((todo) => todo.id !== todoToDelete.id))
  }

  return (
    <div className='app'>
      <div className='input-and-btns'>
        <input type="text" onChange={handleChange} />
        <button onClick={handleAddTodo}>
          Add
        </button>
        <button onClick={handleClick}>
          View
        </button>
      </div>
      {showTodos && <Todos todos={todos} handleDelete={handleDelete} />}
    </div>
  )
}

export default App
