import { useState } from "react"
import './todo.css'
import Done from '../assets/task-done.png'
import NotDone from '../assets/uncheck.png'
import Delete from '../assets/delete.png'

interface Todo {
  todo: {
    id: number,
    todo: string
  },
  handleDelete: any
}

const Todo = ({ todo, handleDelete }: Todo) => {
  const [done, setDone] = useState(false)
  console.log(todo)
  

  return (
    <div className="todo-item">
      <div className="todo">
        <p>{todo.todo}</p>
      </div>
      <div className="todo-config">
        { done 
        ? <button onClick={() => setDone(false)}>
            <img src={Done} />
          </button>
        : <button onClick={() => setDone(true)}>
            <img src={NotDone} />
          </button>
        }
        <button onClick={() => handleDelete(todo)}>
          <img src={Delete} />
        </button>
      </div>
    </div>
  )
}

export default Todo