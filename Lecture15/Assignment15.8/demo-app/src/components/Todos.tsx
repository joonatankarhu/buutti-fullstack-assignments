import Todo from "./Todo"
import './todos.css'

interface TodoInterface {
  id: number,
  todo: string
}

interface Props {
  todos: TodoInterface[],
  handleDelete: any
}

const Todos = (props: Props) => {
  return (
    <ul className="todos-container">
      {props.todos && props.todos.map((todo) => (
        <li key={todo.id}>
          <Todo todo={todo} handleDelete={props.handleDelete} />
        </li>
      ))}
    </ul>
  )
}

export default Todos