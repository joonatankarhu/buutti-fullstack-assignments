## Assignment 15.8: Todo application

Create a React application with an input bar and a button to add a new Todo item, and a view to see all todos in the list
- Each item should be toggleable for done or not done
- Each added item to the list should also be able to be removed / discarded
