const RandomNumber = () => {

  const generateRandomNum = () => {
    const randomNum = Math.floor(Math.random() * 101)
    return randomNum
  }

  return (
    <>
      <p>Your random number is: {generateRandomNum()}</p>
    </>
  )
}

export default RandomNumber