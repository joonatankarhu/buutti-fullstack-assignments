
## Assignment 15.3: Create a Components

Create a new React App, and remove the unnecessary parts, like you did in the previous exercise. Create a new component, **RandomNumber**, which  should generate a random number from 1 to 100, and then render the random number with an informative text.

After creating these the component, modify your **App** component so that it renders a heading and the RandomNumber component.