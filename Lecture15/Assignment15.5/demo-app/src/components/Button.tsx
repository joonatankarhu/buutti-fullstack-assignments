import { useState } from 'react'
import './button.css'

const Button = ({ updateSum }: any) => {
  const [num, setNum] = useState(0)

  const myFunc = () => {
    setNum((num) => num + 1)
    updateSum()
  }

  return (
    <div 
      className="btn" 
      onClick={() => myFunc()}
    >
      {num}
    </div>
  )
}

export default Button