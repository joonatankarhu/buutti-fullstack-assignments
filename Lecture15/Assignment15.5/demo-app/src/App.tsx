import { useState } from 'react'
import Button from './components/Button'
import './app.css'

function App() {
  const [sum, setSum] = useState(0)

  const updateSum = () => {
    setSum((sum) => sum + 1)
  }

  return (
    <div className='btn-container'>
      <Button updateSum={updateSum} />
      <Button updateSum={updateSum} />
      <Button updateSum={updateSum} />
      {sum}
    </div>
  )
}

export default App
