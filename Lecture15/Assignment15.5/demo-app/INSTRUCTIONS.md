
## Assignment 15.5: An Alternating List

Create a component that takes list
```
const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]
```
as an argument and renders each name on its own row so that every other name is bold and the remainders cursive.

**Hint**: Lists can include elements:  `const elementArray = [<b>Hello</b>, <i>Hi</i>]`
