interface Props {
  list: string[]
}

const NameList = (props: Props) => {
  const list = props.list
  
  return (
    <ul>
      {list.map((name, index) => {
        const content = index % 2 !== 0 
        ? <i>{name}</i>
        : <b>{name}</b>
        return <li key={'names' + index}>{content}</li>
      })}
    </ul>
  )
}

export default NameList