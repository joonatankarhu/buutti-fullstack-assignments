import NameList from './components/NameList'

function App() {
  const list: string[] = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]

  return (
    <>
      <NameList list={list} />
    </>
  )
}

export default App
