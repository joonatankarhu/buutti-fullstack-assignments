## Assignment 15.6: Counters

Create an app with three buttons. Each button should have the number zero on them. When a button is clicked, the number in that button should increase by one.
Below the buttons there should also be a number that equals the sum of all the numbers in the buttons.
