import { ChangeEvent, useState } from "react"

const TextInput = () => {
  const [text, setText] = useState('')
  const [showText, setShowText] = useState(false)

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value)
  }

  const handleClick = () => {
    setShowText(true)
  }

  return (
    <>
      <input
        value={text}
        onChange={handleChange}
      />
      <button onClick={handleClick}>
        Show text
      </button>
      {showText && <p>{text}</p>}
    </>
  )
}

export default TextInput