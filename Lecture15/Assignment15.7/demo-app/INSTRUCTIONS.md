## Assignment 15.7: User Input

Create a component with `<input>` and `<button>` elements. When the button is clicked, the inputted text should be displayed in a different element, but only *after* clicking the button.