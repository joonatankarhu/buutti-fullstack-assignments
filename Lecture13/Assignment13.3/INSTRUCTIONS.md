## Assignment 13.3: Connect to Azure

Following the lecture example, connect to the PostgreSQL server in Azure using a containerized version of pgAdmin.
1. Create a new database in the server called "products_api".
2. Create a new table in the database called "products" with three columns: id (serial, primary key), name (varchar) and price (real).
3. Insert one product to the table.
