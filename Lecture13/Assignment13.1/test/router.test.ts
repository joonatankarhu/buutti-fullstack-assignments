import server from "../src/server";
import request from "supertest";
import { jest } from "@jest/globals";
import { pool } from "../src/db";
const initializeMockPool = (mockResponse: any) => {
  (pool as any).connect = jest.fn(() => {
    return {
      query: () => mockResponse,
      release: () => null,
    };
  });
};

describe("Testing GET /products", () => {
  const mockResponse = {
    rows: [
      { id: 101, name: "Test Item 1", price: 100 },
      { id: 102, name: "Test Item 2", price: 200 },
    ],
  };

  beforeAll(() => {
    initializeMockPool(mockResponse);
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
  describe("Server", () => {
    it("Supertest test", async () => {
      const res = await request(server).get("/products");
      expect(res.statusCode).toBe(200);
      expect(res.body).toStrictEqual(mockResponse.rows);
    });
  });
  // the actual tests
});