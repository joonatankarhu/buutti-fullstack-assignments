import express from 'express'
// import { createProductsTable } from './db'
import router from './router'

const app = express()
app.use(express.json())

// createProductsTable()

app.use('/products', router)

export default app