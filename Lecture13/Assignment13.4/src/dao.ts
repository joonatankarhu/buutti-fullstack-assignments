// Database access object

import { executeQuery } from './db'

export const addProduct = async (name: string, price: number) => {
  const query = 'INSERT INTO products (name, price) VALUES ($1, $2)'
  const params = [name, price]

  const result = await executeQuery(query, params)
  console.log(`Created new product:\n${result}`)

  return result
}

export const getProductByName = async (name: string) => {
  const query = 'SELECT * FROM products WHERE name = $1'
  const params = [name]

  const result = await executeQuery(query, params)

  return result
}

export const updateProductById = async (id: number, name: string) => {
  const query = "UPDATE products SET name = $2 WHERE id = $1"
  const params = [id, name]

  const result = await executeQuery(query, params)
  console.log('Updated product to: ', result)

  return result
}

export const deleteProductById = async (id: number) => {
  const query = "DELETE FROM products WHERE id = $1"
  const params = [id]

  const result = await executeQuery(query, params)
  console.log('Deleted product', result)

  return result
}

export const listAllProducts = async () => {
  const query = "SELECT * FROM products"
  const result = await executeQuery(query)
  console.log('Products: ', result)
  return result
}