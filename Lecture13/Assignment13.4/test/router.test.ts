// // Testing products router

// import request from 'supertest'
// import app from '../src/server'
// import { pool } from '../src/db'
// import { jest } from '@jest/globals'

// const initializeMockPool = (mockRes: any) => {
//   (pool as any).connect = jest.fn(() => {
//     return {
//       query: () => mockRes,
//       release: () => null,
//     }
//   })
// }

// describe('test', () => {
//     const mockRes = {
//       rows: [
//         {
//           id: 101,
//           name: 'Test item 1',
//           price: 100,
//         },
//         {
//           id: 102,
//           name: 'Test item 2',
//           price: 200,
//         },
//       ],
//     }
  
//     beforeAll(() => {
//       initializeMockPool(mockRes)
//     })
  
//     afterAll(() => {
//       jest.clearAllMocks()
//     })
  
//     // the tests
//     it('can GET all products', async () => {
//       const res = await request(app).get('/products/all')
//       expect(res.statusCode).toBe(200)
//       expect(res.body).toStrictEqual(mockRes.rows)
//     })
//   })


  import server from "../src/server";
import request from "supertest";
import { jest } from "@jest/globals";
import { pool } from "../src/db";
const initializeMockPool = (mockResponse: any) => {
  (pool as any).connect = jest.fn(() => {
    return {
      query: () => mockResponse,
      release: () => null,
    };
  });
};

describe("Testing GET /products", () => {
  const mockResponse = {
    rows: [
      { id: 101, name: "Test Item 1", price: 100 },
      { id: 102, name: "Test Item 2", price: 200 },
    ],
  };

  beforeAll(() => {
    initializeMockPool(mockResponse);
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
  describe("Server", () => {
    it("Supertest test", async () => {
      const res = await request(server).get("/products");
      expect(res.statusCode).toBe(200);
      expect(res.body).toStrictEqual(mockResponse.rows);
    });
  });
  // the actual tests
});