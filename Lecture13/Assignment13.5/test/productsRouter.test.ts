import { jest } from '@jest/globals'
import { pool } from '../src/db'
import request from 'supertest'
import server from '../src/server'

const initializeMockPool = (mockResponse: any) => {
  (pool as any).connect = jest.fn(() => {
    return {
      query: () => mockResponse,
      release: () => null,
    }
  })
}

describe('Testing GET /products', () => {
  const mockResponse = {
    rows: [
      { id: 101, name: 'Test Item 1', price: 100 },
      { id: 102, name: 'Test Item 2', price: 200 },
    ],
  }

  beforeAll(() => {
    initializeMockPool(mockResponse)
  })

  afterAll(() => {
    jest.clearAllMocks()
  })

  // the actual tests

  it('returns all products', async () => {
    const response = await request(server).get('/products/all')
    expect(response.statusCode).toBe(200)
    expect(response.body).toStrictEqual(mockResponse)
  })

  it('deletes 1 product', async () => {
    const response = await request(server).delete('/products/101')
    expect(response.statusCode).toBe(200)
  })
})
