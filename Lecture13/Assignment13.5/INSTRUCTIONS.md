## Assignment 13.5: Pipeline with PG

Create a CI/CD pipeline that tests and deploys your code when a new version is pushed.
**Extra**: Add a linting stage to the pipeline.