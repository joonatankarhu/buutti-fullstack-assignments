import server from "./server"

const { PORT } = process.env

server.listen(PORT, () => console.log('listening to port', PORT))
