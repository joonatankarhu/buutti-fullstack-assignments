function writeMessage(
  param1: number,
  param2: number,
  param3: number = 4
): number {
  const number = param1 + param2 + param3
  return number
}
console.log(writeMessage(2, 4))

const anomFunc = function (
  param1: number,
  param2: number,
  param3: number = 5
): number {
  const number = param1 + param2 + param3
  return number
}
const sum = anomFunc(5, 10, 15)
console.log(sum)

const arrowFunc = (
  param1: number,
  param2: number,
  param3: number = 10
): number => param1 + param2 + param3

console.log(arrowFunc(20, 20, 20))
