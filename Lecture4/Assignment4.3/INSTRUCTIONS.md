## Assignment 4.3: Function Flavours

Write a function that takes either 2 or 3 parameters, then calculates the sum of these parameters and returns the result.

Do this three times: 
Using a named function
Using an anonymous function
Using an arrow function