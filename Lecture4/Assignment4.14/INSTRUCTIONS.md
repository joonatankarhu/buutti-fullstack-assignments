## Assignment 4.14: Indexed Alphabet
```
const charIndex = { a : 1, b : 2, c : 3, d : 4, e : 5, ... , y : 25, z : 26 }
```

Create a program that turns any given word into charIndex version of the word

Example:  
``npm start bead`` ->  2514  
``npm start rose`` ->  1815195  
