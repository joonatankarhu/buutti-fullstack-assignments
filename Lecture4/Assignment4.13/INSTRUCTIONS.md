## Assignment 4.13: Count Sheep

Create a program that takes in a number from the command line, for example ``npm start 3`` and prints a string ``"1 sheep...2 sheep...3 sheep..."``