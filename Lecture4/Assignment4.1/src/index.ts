const argument = 'hello world'

const func = (param1: string): void => {
  const arr = param1.split(' ')
  const newArr: string[] = arr.map(
    (item) => (item = item.charAt(0).toUpperCase() + item.substring(1))
  )
  const foo = newArr.join(' ')
  console.log(foo)
}

func(argument)

// function myToUpperCase(param1: string) {
//   const words = param1.split(' ')
//   let finalProduct = ''
//   for (let i = 0; i < words.length; i++) {
//     const word = words[i]
//     finalProduct =
//       finalProduct + word[0].toUpperCase() + word.substring(1) + " "
//   }
// }
// myToUpperCase('hello world')
