## Assignment 4.1: toUpperCase

Write a function that takes one string parameter. 

The function should first convert the first letter of each word to upper case, and then print the string.