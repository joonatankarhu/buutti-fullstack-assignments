const start = parseInt(process.argv[2])
const end = parseInt(process.argv[3])

const arr: number[] = []
let num = 0

const printNumbers = (start: number, end: number) => {
  if (start < end) {
    const ascending = (start: number, end: number) => {
      for (let i = start; i <= end; i++) {
        num = i
        arr.push(num)
      }
      console.log(arr)
    }
    ascending(start, end)
  }

  if (start > end) {
    const descending = (start: number, end: number) => {
      for (let i = start; i >= end; i--) {
        num = i
        arr.push(num)
      }
      console.log(arr)
    }
    descending(start, end)
  }
}

printNumbers(start, end)
