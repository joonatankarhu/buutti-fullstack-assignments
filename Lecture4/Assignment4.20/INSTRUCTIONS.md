## Assignment 4.20: Fibonacci Sequence

The Fibonacci sequence is a sequence of numbers where each number is the sum of the two preceding ones, starting from 0 and 1. Create a function that produces a Fibonacci sequence of length **n**.

For example, if n = 8, the program should produce a sequence [0,1,1,2,3,5,8,13].
