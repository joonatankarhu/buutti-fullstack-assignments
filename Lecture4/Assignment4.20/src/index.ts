// First solution
// const n = 8
// const arr: number[] = [0, 1]

// const fibonacci = (n: number, arr: number[]): void => {
// 	let sum
// 	for (let i = 0; i <= n; i++) {
//     sum = arr[i] + arr[i + 1]
// 		arr.push(sum)
// 	}
// 	console.log(arr)
// }
// fibonacci(n, arr)

// second solution
const n = 8
const arr: number[] = []

const fibonacci = (n: number, arr: number[]): void => {
  let sum
  for (let i = 0; i < n; i++) {
    if (arr.length <= 1) {
      sum = 0 + i
      arr.push(sum)
    } else {
      sum = arr[i - 2] + arr[i - 1]
      arr.push(sum)
    }
  }
  console.log(arr)
}
fibonacci(n, arr)
