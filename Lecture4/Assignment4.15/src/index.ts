const competitors = ['Julia', 'Mark', 'Spencer', 'Ann', 'John', 'Joe']
const ordinals = ['st', 'nd', 'rd', 'th']

const result = competitors.map((name, nameIndex) => {
  const nth = nameIndex < 4 ? ordinals[nameIndex] : ordinals[3]
  return `${nameIndex + 1}${nth} competitor was ${name}`
})

console.log(result)
