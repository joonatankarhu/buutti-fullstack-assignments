

## Assignment 4.15: Ordinal Numbers

You have two arrays:
```
const competitors = ['Julia', 'Mark', 'Spencer', 'Ann' , 'John', 'Joe']
const ordinals = ['st', 'nd', 'rd', 'th']
```

Create program that outputs competitors placements with following way:
`['1st competitor was Julia', '2nd competitor was Mark', '3rd competitor was Spencer', '4th competitor was Ann', '5th competitor was John', '6th competitor was Joe']`

