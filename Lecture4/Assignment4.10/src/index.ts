const names = [
  'Murphy',
  'Hayden',
  'Parker',
  'Arden',
  'George',
  'Andie',
  'Ray',
  'Storm',
  'Tyler',
  'Pat',
  'Keegan',
  'Carroll',
]

const findName = names.find(
  (item: string) => item.length === 3 && item.charAt(item.length - 1) === 't'
)

console.log(findName)
