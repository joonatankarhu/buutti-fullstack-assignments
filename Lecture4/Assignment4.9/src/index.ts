const numbers = [
  749385, 498654, 234534, 345467, 956876, 365457, 235667, 464534, 346436,
  873453,
]

function printNumbers(numbers: number[]): number[] {
  const filteredList = numbers.filter(
    (number: number) =>
      number % 3 === 0 ||
      (number % 5 === 0 && number % 3 !== 0 && number % 5 !== 0)
  )
  console.log(filteredList)
  return filteredList
}
printNumbers(numbers)
