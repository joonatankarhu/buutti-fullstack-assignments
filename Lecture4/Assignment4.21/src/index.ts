const n = 4

if (n <= 1 && n % 2 !== 0 && n % n == 0) {
  console.log(`${n} is not a prime number`)
} else {
  console.log(`${n} is a prime number`)
}
