## Assignment 4.21: Prime Numbers

A prime number (or a prime) is a natural number (aka positive integer) greater than 1 that is not a product of two smaller natural numbers. For example, 4 is not a prime because it is divisible by 2. On the other hand, 7 is a prime: it is only divisible by 7 and 1.

Create a function that checks whether a given number is a prime number.