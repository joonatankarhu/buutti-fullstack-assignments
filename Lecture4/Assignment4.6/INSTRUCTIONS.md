## Assignment 4.6: Factorial

Write a program that calculates the **factorial** n of a given number *n* using recursion.

Definition of factorial of n, written as n!, is

n! = n * (n-1) * (n-2) * ... * 1

So for example, if n = 4, then n! = 4 * 3 * 2 * 1 = 24