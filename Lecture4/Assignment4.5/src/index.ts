function sumNumbers(callback: (paramName: number) => void): void {
  let sum = 0
  for (let i = 0; i < 1_000_000_000; i++) {
    if (i % 3 === 0 && i % 5 === 0 && i % 7 === 0) {
      sum += i
    }
  }
  callback(sum)
}

function logger(athingToLog: number): void {
  console.log(athingToLog)
}

sumNumbers(logger)
