## Assignment 4.5: Callback

Create a function that calculates the sum of all the numbers that are 
1. Smaller than a billion (1_000_000_000)
2. Divisible by 3, 5 and 7

The function should take one parameter, a callback function that is called when the computation is done. The callback function should be called with the result as an argument.

Test your code by giving the function a simple arrow function that logs the result.