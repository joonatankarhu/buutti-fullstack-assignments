const words = [
  'city',
  'distribute',
  'battlefield',
  'relationship',
  'spread',
  'orchestra',
  'directory',
  'copy',
  'raise',
  'ice',
]

function printNames(words: string[]): string[] {
  const reversedWords = words.map((word: string) => {
    const splitedString = word.split('')
    const reverseArray = splitedString.reverse()
    const joinArray = reverseArray.join('')
    return joinArray
  })
  console.log(reversedWords)
  return reversedWords
}
printNames(words)
