const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32]
const results: number[] = []

const randomizeArr = (array: number[]) => {
  while (array.length > 0) {
    const randomIndex = Math.floor(Math.random() * array.length)
    const randomElement = array.splice(randomIndex, 1)[0]
    results.push(randomElement)
  }
  console.log(results)
}
randomizeArr(array)
