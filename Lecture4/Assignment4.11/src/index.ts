const arr = ['I', 'like', 'to', 'go', 'swimming']

const arrToString = (arr: string[], spacer: string): string => {
  return arr.reduce((acc, curr) => {
    return acc + spacer + curr
  })
}

console.log(arrToString(arr, ' '))
