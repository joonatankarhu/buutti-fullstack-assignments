## Assignment 4.4: Dices

Your mates are into tabletop RPG's, but have forgotten their dice bags home. Swiftly solve the problem by programming **a dice generator function** that **returns dice functions**. The generator should take in one parameter, the number of sides of the dice. The returned dice function should return a random number between one and the number of sides given.

Generate a six-sided dice, and an eight-sided dice and deal 2d6 + 2d8 damage to the enemy!

**Extra**: Modify your dice functions so that the number of throws is taken as a parameter. E.g. if you want to throw 2d6, you'd just give 2 as an argument to the dice function.