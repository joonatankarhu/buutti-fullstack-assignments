const diceGenerator = (nOfSides: number) => {
  return () => Math.floor(Math.random() * nOfSides) + 1
}

const d6 = diceGenerator(6) // create dice with 6 sides
const d8 = diceGenerator(8) // create dice with 8 sides

const damage = d6() + d6() + d8() + d8()
console.log(damage)
