## Assignment 4.18: Palindrome

Check if given string is a palindrome.

Examples:   
`npm start saippuakivikauppias` -> `Yes, 'saippuakivikauppias' is a palindrome`
`npm start saippuakäpykauppias` -> `No, 'saippuakäpykauppias' is not a palindrome`

