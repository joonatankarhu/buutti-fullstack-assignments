const input = process.argv[2]

const reverseString = input.split('').reverse().join('')

if (input === reverseString) {
  console.log(`Yes, '${input}' is a palindrome`)
} else {
  console.log(`No, '${input}' is not a palindrome`)
}
