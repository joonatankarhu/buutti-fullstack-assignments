## Assignment 4.17: Reversed Words

Create a programs that reverses each word in a string.

`node .\reversed_words.js "this is a very long sentence"` -> `sihT si a yrev gnol ecnetnes`

