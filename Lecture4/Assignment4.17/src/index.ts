const string = process.argv[2]

const reverseStrings = (string: string): void => {
  console.log(string.split(' ').map((word) => word.split('').reverse().join('')).join(' '))
}

reverseStrings(string)
