const names = [
  'rauni',
  'matias',
  'Kimmo',
  'Heimo',
  'isko',
  'Sulevi',
  'Mikko',
  'daavid',
  'otso',
  'herkko',
]

function printNames(names: string[]): string[] {
  const list = names.map((name: string) => {
    return name.charAt(0).toUpperCase() + name.substring(1)
  })
  console.log(list)
  return list
}
printNames(names)
