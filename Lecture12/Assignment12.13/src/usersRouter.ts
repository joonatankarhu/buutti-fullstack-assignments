import express, { Request, Response } from 'express'

const router = express.Router()

interface User {
  id: number,
  username: string
}

const users: User[] = []

router.get('/', (_req: Request, res: Response) => {
  console.log(users)
  
  res.status(200).send(users)
})

router.post('/', (req: Request, res: Response) => {
  const newUser = req.body
  console.log('Adding new user: ', newUser)
  res.status(201).send(`Added new user: ${users}`)
})

export default router
