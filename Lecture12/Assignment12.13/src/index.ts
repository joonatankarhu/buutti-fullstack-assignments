// Express API for the forum database
import express from 'express'
import usersRouter from './usersRouter'
import { createUsersTable } from './db'

const server = express()
server.use(express.json())

createUsersTable()

server.use('/users', usersRouter)

const { PORT } = process.env
server.listen(PORT, () => {
  console.log('Forum API listening to port', PORT)
})
