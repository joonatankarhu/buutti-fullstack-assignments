## Assignment 12.13: Forum API

Create an Express app that servers an API for the forum database. The API should have following endpoints
- users
    - `GET /users` Lists the id's and usernames of all users.
    - `GET /users/:id` Gives detailed user information
    - `POST /users` Adds a new user
- posts
    - `GET /posts` Lists all post id's, poster id's and titles
    - `GET /posts/:id` Sends detailed information of the post, including all the comments associated with the post.
    - `POST /posts` Adds a new post
- comments
    - `GET /comments/:userID` Lists all comments by the user defined by the userId parameter
    - `POST /comment` Adds a new comment

Test your API using Insomnia/Postman.
