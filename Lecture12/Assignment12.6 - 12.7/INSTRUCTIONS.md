
## Assignment 12.6: Node & PostgreSQL

Following the lecture example, create an Express server that connects to your local PostgreSQL instance. The database information should be stored in environment variables. When the server starts, it should create a product table with three columns: id (serial, primary key), name (varchar) and price (real).


## Assignment 12.7: Creating Queries

Continue following the lecture example. Create a router and a database access object to handle
1. Creating a product
2. Reading a product
3. Updating a product
4. Deleting a product
5. Listing all products
