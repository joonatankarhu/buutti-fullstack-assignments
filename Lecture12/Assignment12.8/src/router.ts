import express, { Request, Response } from 'express'
import { addProduct, deleteProductById, getProductByName, listAllProducts, updateProductById } from './dao'

const router = express.Router()

router.post('/', async (req: Request, res: Response) => {
  const { name, price } = req.body

  try {
    await addProduct(name, price)
    res.status(201).send('Added a product')
  } catch (error) {
    console.error(error)
    return res.status(400).send('Error')
  }
})

router.get('/', async (req: Request, res: Response) => {
  const { name } = req.body
  
  try {
    await getProductByName(name)
    res.status(201).send(`Got a product from database by name ${name}`)
  } catch (error) {
    console.error(error)
    return res.status(400).send('Error')
  }
})

router.put('/:id', async (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const { name } = req.body
  
  try {
    await updateProductById(id, name)
    res.status(200).send('Updated product by id')
  } catch (error) {
    console.error(error)
    return res.status(400).send('Error')
  }
})

router.delete('/:id', async (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  
  try {
    await deleteProductById(id)
    res.status(200).send('Deleted product by id')
  } catch (error) {
    console.error(error)
    return res.status(400).send('Error')
  }
})

router.get('/all', async (req: Request, res: Response) => {
  try {
    const result = await listAllProducts()
    
    res.status(200).json(result.rows)
  } catch (error) {
    console.error(error)
    return res.status(400).send('Error')
  }
})

export default router