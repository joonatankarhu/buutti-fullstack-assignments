## Assignment 12.11: Create Content

Create five users.
Create five posts.
Create five comments.


// to create 5 users, repeat this with different values

INSERT INTO users (
	username,
	full_name,
	email
) VALUES (
	'Joona',
	'Joonatan K',
	'joonatan@test.fi'
)

// to create 5 posts, repeat this with different values

INSERT INTO posts (
  user_id,
  title,
  content,
  post_date
) VALUES
  -- Post 1
  (
    1,
    'First Post',
    'This is the content of the first post.',
    '2023-06-22 10:00:00'
  ),
  -- Post 2
  (
    2,
    'Second Post',
    'This is the content of the second post.',
    '2023-06-23 12:30:00'
  ),
  -- Post 3
  (
    3,
    'Third Post',
    'This is the content of the third post.',
    '2023-06-24 15:45:00'
  ),
  -- Post 4
  (
    1,
    'Fourth Post',
    'This is the content of the fourth post.',
    '2023-06-25 09:15:00'
  ),
  -- Post 5
  (
    4,
    'Fifth Post',
    'This is the content of the fifth post.',
    '2023-06-26 16:20:00'
  );


// to create 5 comments

INSERT INTO comments (
  user_id,
  post_id,
  content,
  comment_date
) VALUES
  -- Comment 1
  (
    1,
    1,
    'This is the first comment on Post 1.',
    '2023-06-22 11:30:00'
  ),
  -- Comment 2
  (
    2,
    1,
    'This is the second comment on Post 1.',
    '2023-06-22 14:45:00'
  ),
  -- Comment 3
  (
    3,
    2,
    'This is a comment on Post 2.',
    '2023-06-23 09:00:00'
  ),
  -- Comment 4
  (
    4,
    3,
    'This is a comment on Post 3.',
    '2023-06-24 16:30:00'
  ),
  -- Comment 5
  (
    1,
    2,
    'This is another comment on Post 2.',
    '2023-06-25 12:15:00'
  );
