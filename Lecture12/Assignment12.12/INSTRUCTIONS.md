## Assignment 12.12: Modify Content

Update one email address.
Delete one user.

// Update one email address.

UPDATE users
SET email = 'new_email@example.com'
WHERE id = 1;



// Delete one user.

DELETE FROM users WHERE id = 3
