
## Assignment 12.10: Create Tables

Create three tables: users, posts, and comments. 

The users table should have columns for username, full name, and email. 
The posts table should have columns for the user that posted the comment, title, content and post date.
The comments table should have columns for the user that posted the comment, the post where the comment belongs to, content, and comment date.

Every post in the posts table should have exactly one poster that can be found from the users table.
Every comment in the comments table should have exactly one post that it belongs to.

The idea is that every post has one author, and can have multiple (or none) comments. Comments can be added afterwards without having to change the posts table at all.


-- Create users table
CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" varchar(50) NOT NULL,
  "full_name" varchar(50) NOT NULL,
  "email" varchar(100) UNIQUE NOT NULL
);

-- Create posts table
CREATE TABLE "posts" (
  "id" SERIAL PRIMARY KEY,
  "user_id" INT NOT NULL REFERENCES "users" ("id"),
  "title" varchar(255) NOT NULL,
  "content" varchar NOT NULL,
  "post_date" timestamp NOT NULL
);

-- Create comments table
CREATE TABLE "comments" (
  "user_id" INT NOT NULL REFERENCES "users" ("id"),
  "post_id" INT NOT NULL REFERENCES "posts" ("id"),
  "content" varchar NOT NULL,
  "comment_date" timestamp NOT NULL
);
