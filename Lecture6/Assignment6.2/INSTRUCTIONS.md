## Assignment 6.2: Promise Countdown

Create a countdown program using the setTimeout function and **promises**.

The program should output something like this.

3 ⇒ Wait 1 second
2 ⇒ Wait another second
1 ⇒ Wait the last 1 second..
GO!
