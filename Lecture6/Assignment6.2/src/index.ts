new Promise<number>((resolve) => {
  setTimeout(() => {
    resolve(3)
  }, 1000)
}).then((val) => {
  console.log(val)
  new Promise((resolve) => {
    setTimeout(() => {
      resolve(2)
    }, 1000)
  }).then((val) => {
    console.log(val)
    new Promise((resolve) => {
      setTimeout(() => {
        resolve(1)
      }, 1000)
    }).then((val) => {
      console.log(val)
      new Promise<void>((resolve) => {
        setTimeout(() => {
          resolve()
        }, 1000)
      }).then(() => {
        console.log('GO!')
      })
    })
  })
})
