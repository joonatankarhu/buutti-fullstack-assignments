
## Assignment 6.7: Student Grades

Find the highest, and the lowest scoring students.
Then find the average score of the students.
Print out only the students who scored higher than the average.
Assign grades (1-5) to all students based on their scores
    "1": "1-39",
    "2": "40-59",
    "3": "60-79",
    "4": "80-94",
    "5": "95-100"
```typescript
const students = [
    { name: 'Markku', score: 99 },
    { name: 'Karoliina', score: 58 },
    { name: 'Susanna', score: 69 },
    { name: 'Benjamin', score: 77 },
    { name: 'Isak', score: 49 },
    { name: 'Liisa', score: 89 },
]
```