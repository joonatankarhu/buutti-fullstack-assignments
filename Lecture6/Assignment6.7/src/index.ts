interface student {
  name: string
  score: number
  grade?: number
}

const students: student[] = [
  { name: 'Markku', score: 99 },
  { name: 'Karoliina', score: 58 },
  { name: 'Susanna', score: 69 },
  { name: 'Benjamin', score: 77 },
  { name: 'Isak', score: 49 },
  { name: 'Liisa', score: 89 },
]

const highestStudentScore = students.reduce((acc, curr) => {
  return acc.score > curr.score ? acc : curr
})

const lowestScore = students.reduce((acc, curr) => {
  return acc.score < curr.score ? acc : curr
})

const scoresTotal = students.reduce((a, b) => a + b.score, 0)
const averageScore = scoresTotal / students.length

const assignGrades = (): void => {
  for (let i = 0; i < students.length; i++) {
    if (students[i].score >= 1 && students[i].score <= 39) {
      students[i].grade = 1
    }
    if (students[i].score >= 40 && students[i].score <= 59) {
      students[i].grade = 2
    }
    if (students[i].score >= 60 && students[i].score <= 79) {
      students[i].grade = 3
    }
    if (students[i].score >= 80 && students[i].score <= 94) {
      students[i].grade = 4
    }
    if (students[i].score >= 95 && students[i].score <= 100) {
      students[i].grade = 5
    }
  }
}
assignGrades()

const higherThanAverage = students.filter(
  (student) => student.score > averageScore
)
console.log(higherThanAverage)
