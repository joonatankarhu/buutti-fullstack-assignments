## Assignment 6.6: Largest Number 

1. Create a function that finds the *largest* number in an array. 
2. Create a function that finds the *second largest* number in an array.

Do this assignment without first sorting the array, or using any functions from the Math module or external libraries.
