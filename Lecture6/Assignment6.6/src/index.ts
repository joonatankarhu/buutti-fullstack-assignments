const arr = [3, 6, 2, 5, 89, 32]

const findLargestNum = (arr: number[]) => {
  let largest = arr[0]
  for (let i = 0; i < arr.length; i++) {
    if (largest < arr[i]) {
      largest = arr[i]
    }
  }
  return largest
}
findLargestNum(arr)

const findSecondLargest = (arr: number[]) => {
  let largest = findLargestNum(arr)
  let secondLargest = 0
  for (let i = 0; i < largest; i++) {
    if (arr[i] !== largest && arr[i] > secondLargest) {
      secondLargest = arr[i]
    }
  }
  console.log(secondLargest)
  return secondLargest
}
findSecondLargest(arr)
