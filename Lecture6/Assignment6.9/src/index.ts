const raceLap = async function (): Promise<number> {
  return await new Promise((resolve, reject) => {
    const dice = Math.floor(Math.random() * 101)

    if (dice !== 3) {
      const lapTime = Math.floor(Math.random() * (25 - 20) + 20)
      resolve(lapTime)
    } else {
      reject(new Error('Crashed'))
    }
  })
}

const race = (drivers: string[], laps: number): void => {
  const results: any[] = []
  const promises: Array<Promise<void>> = []

  drivers.forEach((driver) => {
    const person = {
      name: driver,
      times_each_lap: [],
      total_time: 0,
      best_time: 0,
      did_crash: false,
    }

    for (let i = 0; i < laps; i++) {
      if (!person.did_crash) {
        const promise = raceLap()
          .then((value: number) => {
            person.times_each_lap.push(value)
            person.total_time += value
            person.best_time = Math.max(...person.times_each_lap)
            console.log(person)
          })
          .catch((error: Error) => {
            if (!person.did_crash && error.message === 'Crashed') {
              // console.log(`${person.name} has crashed!`)
              person.did_crash = true
            }
          })
        promises.push(promise)
      } else {
        break
      }
    }
    results.push(person)
  })
  Promise.all(promises)
    .then(() => {
      // Find the winner with the shortest total time and no crashes
      const winners: string[] = []
      let shortestTime = Infinity

      for (const person of results) {
        if (!person.did_crash) {
          if (person.total_time < shortestTime) {
            winners.length = 0 // Clear the previous winners
            winners.push(person.name)
            shortestTime = person.total_time
          } else if (person.total_time === shortestTime) {
            winners.push(person.name)
          }
        }
      }

      if (winners.length === 1) {
        console.log(
          `The winner is ${winners[0]} with a total time of ${shortestTime}!`
        )
      } else {
        console.log(
          `It's a tie! The winners are: ${winners.join(
            ', '
          )} with a total time of ${shortestTime}!`
        )
      }
    })
    .catch((error: Error) => {
      console.log('Error occurred:', error)
    })
}
race(['Mila', 'Jere', 'Joonatan'], 4)
