
## Assignment 6.9: Race Cars

Program a race car simulator! 

In this race we have several drivers and several laps. Each lap every driver has a three percent chance of crashing. If they don't crash, they finish one lap in 20-25 seconds.
Write a function **raceLap** that takes no parameters and returns a *promise*. If the driver crashes, the promise is *rejected*. If not, the promise is *resolved* and the lap time is returned. 

Write a function **race** that takes two parameters: a list of drivers (strings), and the number of laps (number). When the race function is run, it should run the **raceLap** function for eachd driver on every lap. The **race** function should keep track of each drivers total time and best lap time. If a driver crashes, their times are not updated on further laps. When all laps have finished, the **race** function should return the name and stats of the winner.
