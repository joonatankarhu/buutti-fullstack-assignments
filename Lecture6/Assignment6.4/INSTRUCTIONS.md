
## Assignment 6.4: Sending API Requests

Install [Postman](https://www.postman.com), [Insomnia](https://insomnia.rest/products/insomnia), or [Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) (VSC Extension). Send some requests to [Cat as a service](https://cataas.com/) API

Get a picture of a random cat
Get a picture of a cat saying "I can haz JavaScript"
Get a JSON list of 20 cat pics
