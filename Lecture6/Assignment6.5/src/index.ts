/* eslint-disable */
import axios from 'axios'

interface Response {
  Title: string
  Year: number
}

const movieSearch = async (title: string, year: number) => {
  const url = `http://www.omdbapi.com/?apikey=c3a0092f&t=${title}&y=${year}`

  const response = await axios.get(url)
  const anyResult = response.data
  const typedResult: Response = response.data

  const movie = `Title: ${typedResult.Title}, Year: ${typedResult.Year}`
  console.log(movie)
}
movieSearch('big', 2022)
