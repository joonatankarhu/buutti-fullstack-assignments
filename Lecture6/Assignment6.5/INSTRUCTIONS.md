
## Assignment 6.5: Movie Search

Create a **movie search function** that uses [Open Movie Database](https://www.omdbapi.com) API. The function should take two parameters, a mandatory title (string) and an optional  year (number).

The movie search should use Axios to make an API call and submit those parameters. The function should use an interface to cast the result to a list of well-typed movie objects. You can choose not to use all the parameters the response has.

Finally, the function should print the results.
