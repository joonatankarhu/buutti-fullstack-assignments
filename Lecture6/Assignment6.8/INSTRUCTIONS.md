## Assignment 6.8: Todos

Use the axios library (or fetch) to fetch data from ‘https://jsonplaceholder.typicode.com/todos/’
1. Console log out that data
2. Modify the existing data by also fetching the user from ‘https://jsonplaceholder.typicode.com/users/${userId}’ and adding it to the post where the userId is from, and remove the userId from that data item
3. Modify in the resulting array of objects the ‘user’ field of every object to only contain the fields ‘name’, ‘username’, and ‘email’.
4. Modify the functionality so that you separately fetch all users from 'https://jsonplaceholder.typicode.com/users/' and all todos from 'https://jsonplaceholder.typicode.com/todos/', and solve the tasks 2-4 by using only the data from the two arrays you fetched, effectively reducing the amount of requests to the API to 2.

Example of data object array after task 2
```javascript
[
...
  {
    userId: 3,
    id: 41,
    title: 'aliquid amet impedit consequatur aspernatur placeat eaque fugiat suscipit',
    completed: false,
    user: {
      id: 3,
      name: 'Clementine Bauch',
      username: 'Samantha',
      email: 'Nathan@yesenia.net',
      address: [Object],
      phone: '1-463-123-4447',
      website: 'ramiro.info',
      company: [Object]
    }
  },
  ...
]
```

and after task 4:
```javascript
[
...
  {
    userId: 3,
    id: 41,
    title: 'aliquid amet impedit consequatur aspernatur placeat eaque fugiat suscipit',
    completed: false,
    user: {
      name: 'Clementine Bauch',
      username: 'Samantha',
      email: 'Nathan@yesenia.net',
    }
  },
  ...
]
```
