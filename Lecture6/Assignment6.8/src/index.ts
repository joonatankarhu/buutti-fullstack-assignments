const todos = async (): Promise<void> => {
  const todos = await fetch('https://jsonplaceholder.typicode.com/todos/').then(
    async (res) => await res.json()
  )
  const users = await fetch('https://jsonplaceholder.typicode.com/users/').then(
    async (res) => await res.json()
  )

  const result = todos.map((todo): void => {
    const currentUser = users.find((user) => user.id === todo.userId)
    todo = {
      ...todo,
      user: {
        name: currentUser.name,
        username: currentUser.username,
        email: currentUser.email,
      },
    }
    return todo
  })
  console.log(result)
}
todos()
