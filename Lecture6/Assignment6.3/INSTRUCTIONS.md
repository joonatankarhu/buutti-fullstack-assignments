
## Assignment 6.3: Random Async

Use the following asynchronous function twice to get 2 random values. After getting both values, console.log() them.

```typescript
const getValue = function (): Promise<number> {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(Math.random())
        }, Math.random() * 1500)
    })
}
```

Do this exercise **twice**. First time, use *async & await*, and on the second time use *promise.then()*.
