const getValue = async function (): Promise<number> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(Math.random())
    }, Math.random() * 1500)
  })
}
getValue()

const values = async () => {
  const val1 = await getValue()
  const val2 = await getValue()
  console.log(val1, val2)
}
values()

getValue().then((res1) => {
  getValue().then((res2) => {
    console.log(res1, res2)
  })
})
