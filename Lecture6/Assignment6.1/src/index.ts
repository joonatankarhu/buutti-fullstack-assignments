
function countDown (time: number, func: () => void) {
  setTimeout(() => {
    func()
  }, time)
}

countDown(1000, () => {
  console.log('Wait 1 second')
  countDown(1000, () => {
    console.log('Wait another second')
    countDown(1000, () => {
      console.log('Wait the last 1 second...')
      countDown(1000, () => {
        console.log('GO!')
      })
    })
  })
})
