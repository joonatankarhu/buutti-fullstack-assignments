
## Assignment 6.1: Callback Countdown

Create a countdown program using the setTimeout function and **callbacks**.

The program should output something like this.

3   ⇒ Wait 1 second
2   ⇒ Wait another second
1   ⇒ Wait the last 1 second..
GO!