
## Assignment 5.15: Collatz Conjecture

Define a number n that is larger than 0, for example ``n = 3``

Create a function that **given parameter n finds the number of steps it takes to reach number 1 (one)** using the following process

- **If n is even, divide it by 2**  
- **If n is odd, multiply it by 3 and add 1**  

Example: 

For ``n = 3`` the process would be following
0: n<sub>0</sub> = 3
1: 3 is odd, so we multiply by three and add one. n<sub>1</sub> = n<sub>0</sub> * 3 + 1 = 10
2: 10 is even, so we divide by two. n<sub>2</sub> = n<sub>1</sub> / 2 = 5
3: n<sub>3</sub> = n<sub>2</sub> * 3 + 1 = 16
4: n<sub>4</sub> = n<sub>3</sub> / 2 = 8
5: n<sub>5</sub> = n<sub>4</sub> / 2 = 4
6: n<sub>6</sub> = n<sub>5</sub> / 2 = 2
7: n<sub>7</sub> = n<sub>6</sub> / 2 = 1
So we reach n = 1 after seven steps.

You can read more about the **Collatz conjecture** from [Wikipedia](https://en.wikipedia.org/wiki/Collatz_conjecture) or [XKCD](https://xkcd.com/710/).

