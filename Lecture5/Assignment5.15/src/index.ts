const n = 3

const findNumberOfSteps = (n: number): void => {
  if (n % 2 === 0) {
    n = n / 2
    console.log(n)
    findNumberOfSteps(n)
  } else if (n !== 1) {
    n = n * 3 + 1
    console.log(n)
    findNumberOfSteps(n)
  }
}

findNumberOfSteps(n)