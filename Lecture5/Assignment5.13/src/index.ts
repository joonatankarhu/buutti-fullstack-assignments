const checkExam = (
  correctAnswers: string[],
  studentAnswers: string[]
): void => {
  let score = 0
  for (let i = 0; i < 4; i++) {
    if (correctAnswers[i] === studentAnswers[i]) {
      score = score + 4
    }
    if (correctAnswers[i] !== studentAnswers[i]) {
      score = score - 1
    }
    if (studentAnswers[i] === '') {
      score = score + 0
    }
  }

  if (score < 0) {
    console.log(0)
  } else {
    console.log(score)
  }
}

// checkExam(['a', 'a', 'b', 'b'], ['a', 'c', 'b', 'd']) // 6
checkExam(['a', 'a', 'b', 'c'], ['a', 'a', 'b', 'c']) // 16
