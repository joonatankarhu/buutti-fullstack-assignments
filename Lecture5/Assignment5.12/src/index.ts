const getVowelCount = (string: string): void => {
  const vowels = ['a', 'e', 'i', 'o', 'u', 'y']
  const split = string.split('')
  const result = split.filter((char) => vowels.includes(char))
  const count = result.length
  console.log(count)
}

getVowelCount('orange')
