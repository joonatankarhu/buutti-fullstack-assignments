## Assignment 5.12: Count Vowels

Return the number (count) of vowels in the given string. Let's consider ``a, e, i, o, u, y`` as vowels in this exercise.

```javascript
getVowelCount('abracadabra') // 5
```
