
## Assignment 5.10: Likes

Create a "like" function that takes in a array of names. Depending on a number of names (or length of the array) the function must return strings as follows:

```javascript
likes([]); // "no one likes this"
likes(["John"]) // "John likes this"
likes(["Mary", "Alex"]) // "Mary and Alex like this"
likes(["John", "James", "Linda"]) // "John, James and Linda like this"
likes(["Alex", "Linda", "Mark", "Max"]) // must be "Alex, Linda and 2 others 
```
For 4 or more names, ``"2 others"`` simply increases.
