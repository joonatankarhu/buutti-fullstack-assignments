const aboveAverage = (numbers: number[]): void => {
  const sum = numbers.reduce((a, b) => a + b, 0)
  const avg = sum / numbers.length || 0
  const filteredNumbers = numbers.filter((number) => number > avg)
  console.log(filteredNumbers)
}

aboveAverage([1, 5, 9, 3])
