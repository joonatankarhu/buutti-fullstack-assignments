## Assignment 5.11: Above Average

Create a function that takes in an array and returns a new array of values that are above average.

```javascript 
aboveAverage([1, 5, 9, 3]) // [5, 9]
```
