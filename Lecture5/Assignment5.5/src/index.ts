class Ingredient {
  name: string
  amount: number

  constructor(name: string, amount: number) {
    this.name = name
    this.amount = amount
  }

  scale(num: number): void {
    this.amount = num * this.amount
  }
}

const strawberries = new Ingredient('strawberries', 8)
const flour = new Ingredient('flour', 3)
const blueberries = new Ingredient('blueberries', 5)

flour.scale(10)

class Recipe {
  name: string
  ingredients: Array<{ name: string; amount: number }>
  servings: number

  constructor(name: string, ingredients: Ingredient[], servings: number) {
    this.name = name
    this.ingredients = ingredients
    this.servings = servings
  }

  toString(): string {
    return this.ingredients.reduce(
      (acc: string, cur: { name: string; amount: number }) => {
        return acc + `- ${cur.name} (${cur.amount})\n`
      },
      `${this.name} (${this.servings} servings)\n\n`
    )
  }
}

const pie = new Recipe('Pie', [strawberries, flour, blueberries], 2)
console.log(pie.toString())

class HotRecipe extends Recipe {
  heatLevel: number

  constructor(
    name: string,
    ingredients: Ingredient[],
    servings: number,
    heatLevel: number
  ) {
    super(name, ingredients, servings)
    this.heatLevel = heatLevel
  }

  toString() {
    if (heatLevel > 5) {
      this.warning = 'Warning, it is too hot!'
    }
  }
}

console.log(HotRecipe(9))
