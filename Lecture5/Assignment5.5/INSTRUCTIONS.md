## Assignment 5.5: Extended Recipes

Your Aunt Annie is super impressed by your recipes, so she wants to get some use of them as well. She runs a mexican restaurant that has some very hot recipes so she needs a bit extended version.

1. Create a *HotRecipe* **class** that **extends** the *Recipe* you have already created. *HotRecipe* should have a *heatLevel* parameter in addition to the *Recipe* parameters
2. *Override* the existing *toString* method with a new one that adds a warning to the recipe if the *heatLevel* is over 5.

Test your code. Create a new HotRecipe object with heatLevel 3 and another with heatLevel 8. Scale both to 100 portions and print the results.