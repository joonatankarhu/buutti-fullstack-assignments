import fs from 'fs'

const data = fs.readFileSync('./src/sampleText.txt', 'utf-8')

const split = data.split(' ')

const filteredArr = split.map((item) => {
  if (item === 'joulu' || item === 'Joulu') {
    item = 'Kinkku'
    return item
  }
  if (item === 'lapsilla' || item === 'Lapsilla') {
    item = 'Poroilla'
    return item
  }
  return item
})

const result = filteredArr.join(' ')

fs.writeFileSync('./src/sampleText.txt', result)
console.log('The written has the following contents:')
console.log(fs.readFileSync('./src/sampleText.txt', 'utf8'))
