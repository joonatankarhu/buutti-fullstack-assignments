
## Assignment 5.7: Read/Write File

1. Implement a program that can read your given file, and print out all the information in it.

2. Print out word for word, and every time the word you’re trying to print is “joulu”, you replace that word with “kinkku”, and every time your printing “lapsilla” you print “poroilla”. 

3. Finally, write the altered text to a new file. 


For this exercise, create a text file with this content:
```
Joulu on taas, joulu on taas, kattilat täynnä puuroo. Nyt sitä saa, nyt sitä saa vatsansa täyteen puuroo.
Joulu on taas, joulu on taas, voi, kuinka meill on hauskaa! Lapsilla on, lapsilla on aamusta iltaan hauskaa.
```