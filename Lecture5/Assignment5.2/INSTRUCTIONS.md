
## Assignment 5.2: Objective Recipes

Your uncle Anton has just started a new restaurant and needs to keep track of all the recipes in the upcoming menu. Let's build some recipe objects to help him stay in business!

If you are using TypeScript, *do not include interfaces* in this exercise. You are allowed to be loose with the type definitions on this assignment.

First, we need some ingredients. Create an **object constructor** that creates ingredient objects. Every ingredient should have two properties: name (string) and amount (number). Don't worry about type checking.

Next we need the actual recipes. Create an **object constructor** that creates recipe objects. Every recipe should have three properties: name (string), ingredients (list of ingredient objects) and servings (number).

Finally we want to be able to get a nicely formatted version of the recipe for printing. Add a *toString* **method** that returns a string with the recipe name, amount of servings the recipe provides, and all the ingredients and amounts as a list.

Test the recipe object creator by first creating some ingredient objects, then creating a recipe object from those ingredients and printing the recipe using the toString method.
