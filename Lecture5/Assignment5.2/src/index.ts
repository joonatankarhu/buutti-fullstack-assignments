function Ingredient(name: string, amount: number) {
  this.name = name
  this.amount = amount
}
const strawberries = new Ingredient('strawberries', 8)
const flour = new Ingredient('flour', 3)
const blueberries = new Ingredient('blueberries', 5)

const ingredients = [strawberries, flour, blueberries]

function Recipe(
  name: string,
  ingredients: {
    name: string
    amount: number
  }[],
  servings: number
) {
  this.name = name
  ;(this.ingredients = ingredients), (this.servings = servings), toString()
}
const pie = new Recipe('Pie', ingredients, 2)
console.log(pie)
