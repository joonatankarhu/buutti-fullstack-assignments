import fs from 'fs'

const fileContents = fs.readFileSync('./src/forecast_data.json', 'utf-8')

const jsonFormat = JSON.parse(fileContents)
jsonFormat.temperature = 30

const jsonString = JSON.stringify(jsonFormat)
fs.writeFileSync('./src/forecast_data.json', jsonString, 'utf-8')
