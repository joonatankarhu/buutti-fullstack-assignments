
## Assignment 5.8: Forecast

Create forecast_data.json and then add to following data to
```
{
  "day": "monday",
  "temperature": 25,
  "cloudy": true,
  "sunny": false,
  "windy": false
}
```

Then retrieve the data and modify the temperature of that forecast. Lastly, save the change back to the file.

