## Assignment 5.4: Classy Recipes

Uncle Anton did not accept the program. He said prototypes and function object constructors are so last century.

Refactor your code to use class notation. It should work just as it did before.