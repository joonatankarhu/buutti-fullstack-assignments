
## Assignment 5.6: Modules

Create a new node project. Remember to create the necessary scripts as well! If you are using TypeScript, you also need to install typescript package as dependency.

Create a module file called MyModule.js (or .ts)

Inside your module, create a function helloModule, that prints "Hello Module!". Declare this function as the default export.

Also create constants for pi (3.1) and Euler's number, e, (2.7). Declare these as named exports.

Import all these to your index.js (.ts) file and print the hello message as well as both constants.