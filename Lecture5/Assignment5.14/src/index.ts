const generateCredentials = (firstName: string, lastName: string): void => {
  const currentYear = new Date().getFullYear()
  const lastCharsOfYear = currentYear.toString().slice(-2)

  const randomAtoZIndex = Math.floor(Math.random() * (90 - 65) + 65)
  const randomAtoZLetter = String.fromCharCode(randomAtoZIndex)

  const randomSpecialCharIndex = Math.floor(Math.random() * (47 - 33) + 33)
  const randomSpecialCharacter = String.fromCharCode(randomSpecialCharIndex)

  const firstTwoCharOfName = firstName.substring(0, 2)
  const firstTwoCharOfLastName = lastName.substring(0, 2)

  const firstCharFirstName = firstName.charAt(0).toLowerCase()
  const lastCharLastName = lastName.charAt(lastName.length - 1).toUpperCase()

  const username = `B${lastCharsOfYear}${firstTwoCharOfLastName.toLowerCase()}${firstTwoCharOfName.toLowerCase()}`
  const password = `${randomAtoZLetter}${firstCharFirstName}${lastCharLastName}${randomSpecialCharacter}${lastCharsOfYear}`

  build(username, password)
}

const build = (username: string, password: string): void => {
  const userAndPass = []
  userAndPass.push(username, password)
  console.log(userAndPass)
}

generateCredentials('John', 'Doe')
