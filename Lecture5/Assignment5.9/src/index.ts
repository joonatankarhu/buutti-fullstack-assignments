function calculator(operator: string, num1: number, num2: number): void {
  if (operator === '+') {
    const sum = num1 + num2
    console.log(sum)
  }
  if (operator === '-') {
    const sum = num1 - num2
    console.log(sum)
  }
  if (operator === '/') {
    const sum = num1 / num2
    console.log(sum)
  }
  if (operator === '*') {
    const sum = num1 * num2
    console.log(sum)
  }
  if (
    operator !== '+' &&
    operator !== '-' &&
    operator !== '/' &&
    operator !== '*'
  ) {
    console.log("Can't do that!")
  }
}

calculator('-', 10, 30)
