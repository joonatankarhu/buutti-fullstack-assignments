

## Assignment 5.9: Calculator

Create a function that takes in an operator and two numbers and returns the result.

```javascript
function calculator(operator, num1, num2) {
    // your code
}

calculator("+", 2, 3) // returns 5
```

For example, if the operator is "+", sum the given numbers. In addition to sum, calculator can also calculate differences, multiplications and divisions. If the operator is something else, return some error message like ``"Can't do that!"``
