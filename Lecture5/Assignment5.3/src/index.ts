function Ingredient(name: string, amount: number): void {
  this.name = name
  this.amount = amount
}

Ingredient.prototype.scale = function (num: number) {
  // eslint-disable-next-line @typescript-eslint/no-unused-expressions
  this.amount = num * this.amount
}

const strawberries = new Ingredient('strawberries', 8)
const flour = new Ingredient('flour', 3)
const blueberries = new Ingredient('blueberries', 5)

flour.scale(10)

function Recipe(
  name: string,
  ingredients: Array<{ name: string; amount: number }>,
  servings: number
): void {
  this.name = name
  this.ingredients = ingredients
  this.servings = servings
}

Recipe.prototype.toString = function () {
  return this.ingredients.reduce(
    (acc: string, cur: { name: string; amount: number }) => {
      return acc + `- ${cur.name} (${cur.amount})\n`
    },
    `${this.name} (${this.servings} servings)\n\n`
  )
}

const pie = new Recipe('Pie', [strawberries, flour, blueberries], 2)
console.log(pie.toString())
