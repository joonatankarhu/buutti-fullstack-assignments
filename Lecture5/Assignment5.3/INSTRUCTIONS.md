
## Assignment 5.3: Prototype Recipes

If you are using TypeScript, *do not include interfaces* in this exercise. You are allowed to be loose with the type definitions on this assignment.

Let’s use our newfound knowledge to improve our recipes.

1. **Refactor** your code so that the *toString* method is declared in the **prototype** instead of the object constructor.
2. Add a scale method to the *Ingredient* **prototype** that scales the *amount* property by a given factor.
3. Add a *setServings* **method** to the *Recipe* **prototype** that 
    - scales each ingredient according to the difference 
between new and old servings values
    - sets the servings parameter to the new value

Test your code. Create some new Ingredient objects, then a new Recipe object, scale it to 100 servings, and print the resulting recipe.

