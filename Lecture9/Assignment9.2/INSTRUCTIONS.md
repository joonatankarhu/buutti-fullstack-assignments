## Assignment 9.2: Students Router

Let's continue improving the Students API we created on the last lecture.

Add a `studentRouter.js` file that exports a **router** with all the five routes the app currently has.