import express, { Request, Response } from 'express'
import { middlewareLogger, errorHandler } from './middleware'
import students from './studentRouter'

const server = express()
const port = 3000
server.use(express.json())
server.use(middlewareLogger)
server.use('/students', students)

server.use(express.static('public'))

server.get('/', (_req: Request, res: Response) => {
  res.send('ok')
})


server.use(errorHandler)

server.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
