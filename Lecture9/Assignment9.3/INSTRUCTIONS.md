## Assignment 9.3: Registration

Create and attach a new router, `userRouter.js` that has a single endpoint: ` POST /register` .

The endpoint should 
- expect a request body with two parameters, *username* and *password*.
- create a hash from the password using the **Argon2 library**. 
- store the username and the hash in an **in-memory storage** (e.g. let users = [ ... ]) and log the result to the console. 
- return a response with status code **201 (Created)** on success
