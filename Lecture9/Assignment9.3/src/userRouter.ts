import express, { Request, Response } from 'express'
import argon from 'argon2'

const router = express.Router()

interface User {
  username: string,
  password: string
}

const users: Array<User> = []

router.post('/', (req: Request, res: Response) => {

  const { username, password } = req.body

  const createUser = async () => {
    const hashedPass = await argon.hash(password)
    const user = {
      username,
      password: hashedPass
    }
    users.push(user)
    console.log(users)
    res.send(201)
  }
  createUser()

})

export default router
