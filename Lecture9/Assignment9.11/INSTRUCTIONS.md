## Assignment 9.11: Two Levels of Users

Add a hardcoded user, *admin*, to the Books API. Admin user should exist when the program starts, and nobody should be able to register a new user with this username. The password hash **must not** be hard-coded to the program.

When admin user logs in, the returned signed token should include a parameter `isAdmin: true`.

Secure the *modifying routes* (POST, PUT, DELETE) in the books router so that they can only be accessed if the user is an admin. The other routes (GET) should still be accessible by all registered users.
