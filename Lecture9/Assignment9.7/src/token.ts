import 'dotenv/config'
import jwt from 'jsonwebtoken'

const token = process.env.TOKEN ?? ''
const secret = process.env.SECRET ?? ''

const verifyToken = jwt.verify(token, secret)

console.log(verifyToken)