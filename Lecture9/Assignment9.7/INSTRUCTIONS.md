## Assignment 9.7: Verify a Token

Write a simple **command line program** that verifies JSON Web tokens.

The program should print the contents of the verified token. If the token is not valid, the program should print an error message and exit gracefully.

Create a JWT in [https://jwt.io](https://jwt.io) debugger. Remember to set your secret value. Use your program to verify the token and see that the data entered is as it should be.
