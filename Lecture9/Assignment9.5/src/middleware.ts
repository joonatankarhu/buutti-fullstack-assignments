import { Request, Response, NextFunction } from 'express'

export const middlewareLogger = (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  console.log('Request body: ', req.body)

  const time = new Date()
  const method = req.method
  const url = req.url
  console.log({ reqTime: time, reqMethod: method, reqUrl: url })

  next()
}

export const errorHandler = (
  _req: Request,
  res: Response,
  _next: NextFunction
) => {
  res.status(404).send()
}

//   error: any,
//   _req: Request,
//   res: Response,
//   _: NextFunction
// ) => {
//   console.log("HELLO");
//   res.status(404).send("DJJFJFJFFJ");
// };
