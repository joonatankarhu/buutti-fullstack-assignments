import express, { Request, Response } from 'express'
import argon from 'argon2'
import 'dotenv/config'

const router = express.Router()

interface User {
  username: string
  password: string
}

const users: Array<User> = []

router.post('/', async (req: Request, res: Response) => {
  const { username, password } = req.body

    const hashedPass = await argon.hash(password)
    const user = {
      username,
      password: hashedPass,
    }
    users.push(user)
    console.log(users)
    res.send(201)
})

router.post('/login', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const user = users.find((user) => user.username === username)
  if (user) {
    const verifyPass = await argon.verify(user.password, password)
    console.log(verifyPass)
    res.status(204).send()
  }

  if (!user) {
    res.status(401).send()
  }
})

router.post('/admin', async (req: Request, res: Response) => {
  const { username, password } = req.body
  const adminUser = process.env.ADMIN ?? ''
  const adminHash = process.env.ADMIN_HASH ?? ''

  
  if(username !== adminUser) {
    return res.status(401).send()
  }
  
  const passVerified = await argon.verify(adminHash, password)
  if (!passVerified) {
    return res.status(401).send()
  }

  res.status(204).send()

})

export default router
