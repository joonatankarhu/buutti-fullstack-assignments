import express, { Request, Response } from 'express'
import argon from 'argon2'

const router = express.Router()

interface User {
  username: string
  password: string
}

const users: Array<User> = []

router.post('/', (req: Request, res: Response) => {
  const { username, password } = req.body

  const createUser = async () => {
    const hashedPass = await argon.hash(password)
    const user = {
      username,
      password: hashedPass,
    }
    users.push(user)
    console.log(users)
    res.send(201)
  }
  createUser()
})

router.post('/login', (req: Request, res: Response) => {
  const { username, password } = req.body

  const login = async () => {
    const user = users.find((user) => user.username === username)
    if (user) {
      const verifyPass = await argon.verify(user.password, password)
      console.log(verifyPass)
      res.status(204).send()
    }

    if (!user) {
      res.status(401).send()
    }
  }
  login()
})

export default router
