## Assignment 9.1: Static

Let's continue improving the Students API we created in the last lecture.

Add a **static** info page to your API. 

The page should be reachable from the root path ` / ` and it should include some information about all the endpoints in the API.