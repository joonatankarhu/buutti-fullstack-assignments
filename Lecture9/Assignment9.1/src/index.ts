import express, { Request, Response } from 'express'
import { middlewareLogger, errorHandler } from './middleware'

const server = express()
const port = 3000
server.use(express.json())
server.use(middlewareLogger)

server.use(express.static('public'))

server.get('/', (_req: Request, res: Response) => {
  res.send('ok')
})

server.get('/students', (_req: Request, res: Response) => {
  const studentId = studentList.map((student) => student.id)
  res.send(studentId)
})

let studentList: Array<any> = []

server.post('/student', (req: Request, res: Response) => {
  const { id, name, email } = req.body
  type Student = {
    id: number
    name: string
    email: string
  }
  const student: Student = {
    id,
    name,
    email,
  }
  studentList.push(student)
  console.log('studentList', studentList)
  res.status(201).send(studentList)
})

server.get('/student/:id', (req: Request, res: Response) => {
  const id = Number(req.params.id)
  const data = studentList.find((stud) => stud.id === id)
  if (data === undefined) {
    res.status(404).send(studentList + 'Error: Student not found.')
  }
  res.send(data)
})

server.put('/student/:id', (req: Request, res: Response) => {
  const { name, email } = req.body
  const id = parseInt(req.params.id)
  const studentIndex = studentList.findIndex((student) => student.id === id)

  if (!studentList[studentIndex]) {
    res.status(404).send()
    return
  }

  if (!name && !email) {
    res.status(400).send('Error: name or email are missing')
  } else if (name || email) {
    if (name) {
      const studentIndex = studentList.findIndex((student) => student.id === id)

      if (studentIndex !== -1) {
        // Checks so the student exists in the array with a arr index. Then updates name to a new name
        studentList[studentIndex] = { ...studentList[studentIndex], name: name }
      } else {
        console.log('Item not found in the array.')
      }
    }
    if (email) {
      const studentIndex = studentList.findIndex((student) => student.id === id)

      if (studentIndex !== -1) {
        // Checks so the student exists in the array with a arr index. Then updates email to a new email
        studentList[studentIndex] = {
          ...studentList[studentIndex],
          email: email,
        }
      } else {
        console.log('Item not found in the array.')
      }
    }
    console.log(`Updated student list: ${studentList}`)

    res.status(204).send()
  }
})

server.delete('/student/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const studentIndex = studentList.findIndex((student) => student.id === id)

  if (!studentList[studentIndex]) {
    res.status(404).send()
    return
  }
  const deletedStudentName = studentList[studentIndex].name
  studentList = studentList.splice(studentIndex, 1)
  console.log(`Student ${deletedStudentName} has been deleted from the list.`)
  res.status(204).send()
})

server.use(errorHandler)

server.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
