import express from 'express'
import helmet from 'helmet'


import { notFound, logging } from './middleware'

import userRouter from './userRouter'
import bookRouter from './bookRouter'

const app = express()

app.use(helmet())
app.use(express.json())

app.use('/api/v1/users', userRouter)
app.use('/api/v1/books', bookRouter)


// app.use(logging)

app.use(notFound)

const PORT = process.env.PORT

app.listen(PORT, () => console.log('Listening on port 3000'))
