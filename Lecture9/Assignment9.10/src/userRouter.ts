import 'dotenv/config'
import express, { Request, Response } from 'express'
import argon from 'argon2'
import jwt from 'jsonwebtoken'

interface User {
  username: string
  password: string
}

const users: Array<User> = []

const router = express.Router()

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.post('/register', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const userFromList = users.find((user) => user.username === username)

  if (userFromList === undefined) {
    const hash = await argon.hash(password)

    const newUser = {
      username,
      password: hash,
    }
    users.push(newUser)

    const payload = { username: username }
    const secret = process.env.AUTHENTICATE_SECRET ?? ''
    const options = { expiresIn: '1h'}

    const token = jwt.sign(payload, secret, options)

    res.send(token)
  } else {
    res.send('Something went wrong, when trying to register...')
  }
})

router.post('/login', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const userFromList = users.find((user) => user.username === username)

  if (userFromList !== undefined) {
    const hashedPassword = userFromList.password

    const passwordsMatch = await argon.verify(hashedPassword, password)

    if (passwordsMatch) {

      const payload = { username: username }
      const secret = process.env.AUTHENTICATE_SECRET ?? ''
      const options = { expiresIn: '1h'}

      const token = jwt.sign(payload, secret, options)

      res.send(token)
    }
  } else {
    console.log('user does not exist')
    res.status(401).send('Error: Wrong username or password')
  }
})

export default router
