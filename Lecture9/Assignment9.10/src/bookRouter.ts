import express, { Request, Response } from 'express'
import { checkReqParams, authenticate } from './middleware'

interface Book {
  id: number
  name: string
  author: string
  read: boolean
}

interface CustomRequest extends Request {
  user?: any
}

let books: Array<Book> = []

const router = express.Router()

router.use(authenticate)

router.post('/', checkReqParams, (req: CustomRequest, res: Response) => {
  const newBook: Book = req.body
  console.log('DEBUG', req.user)
  

  books.push(newBook)
  console.log(books)

  res.status(201).send('Created a book')
})

router.put('/:id', checkReqParams, (req: Request, res: Response) => {
    const id = parseInt(req.params.id)

    if (books.length > 0) {
      const newBook: Book = req.body

      const findBookById = books.filter((book) => book.id === id)
      const book = findBookById[0]

      const editBook = (book: any, newBook: any): void => {
        if (book !== newBook) {
          book.name === newBook.name
            ? null
            : (book = { ...book, name: newBook.name })
          book.author === newBook.author
            ? null
            : (book = { ...book, author: newBook.author })
          book.read === newBook.read
            ? null
            : (book = { ...book, read: newBook.read })

          const index = books.findIndex((book) => book.id === id)
          if (index !== -1) {
            books[index] = book
            res.status(201).send(books)
          }
        } else {
          res.status(400).send('No changes were made')
        }
      }

      editBook(book, newBook)
    }
  }
)

router.get('/', (_req: Request, res: Response) => {
  if (books.length < 1 || undefined) {
    res.status(404).send('Error: there are no books in the list...')
  }
  res.status(200).send(books)
})

router.get('/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const targetBook = books.filter((book) => book.id === id)
  res.status(200).send(targetBook[0])
})

router.delete('/:id', (req: Request, res: Response) => {
  const id = parseInt(req.params.id)
  const index = books.findIndex((book) => book.id === id)
  if (index !== -1) {
    books = books.splice(index - 1, 1)
    console.log(books)
    res.status(200).send('Removed book')
  }
})

export default router
