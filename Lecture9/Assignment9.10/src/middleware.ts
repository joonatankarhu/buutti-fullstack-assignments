import { Request, Response, NextFunction } from 'express'
import jwt from 'jsonwebtoken'

interface CustomRequest extends Request {
  user?: any
}


export const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
  const auth = req.get('Authorization')
  if (!auth?.startsWith('Bearer ')) {
    return res.status(401).send('Invalid token')
  }

  const token = auth.substring(7)

  const secret = process.env.AUTHENTICATE_SECRET ?? ''

  
  try {
    const decodedToken = jwt.verify(token, secret)
    req.user = decodedToken

    next()
  } catch (error) {
    return res.status(401).send('Invalid token')
  }
}

export const checkReqParams = (req: Request, res: Response, next: NextFunction) => {
  const body = req.body
  const { name, author, read } = body
  if (!name || !author || !read) {
    res.status(400).send('Invalid or missing parameters!')
  } else {
    next()
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const notFound = (_req: Request, res: Response, _next: NextFunction) => {
  res.status(404).send('Not Found')
}

export const logging = (req: Request, _res: Response, next: NextFunction) => {
  const data = {
    date: new Date().toISOString(),
    method: req.method,
    headers: req.headers,
    params: req.params,
    query: req.query,
    url: req.url,
    request_body: req.body,
  }

  console.log(data)

  next()
}
