import express from 'express'
import helmet from 'helmet'

import { notFound, logging } from './middleware'

import userRouter from './userRouter'
import bookRouter from './bookRouter'


const server = express()

server.use(helmet())
server.use(express.json())

server.use('/api/v1/users', userRouter)
server.use('/api/v1/books', bookRouter)

// server.use(logging)

server.use(notFound)

export default server