import server from './server'
import 'dotenv/config'

const PORT = process.env.PORT

server.listen(PORT, () => console.log('Listening on port 3000'))
