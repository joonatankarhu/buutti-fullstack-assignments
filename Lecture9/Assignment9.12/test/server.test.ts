import request from 'supertest'
import server from '../src/server'

describe('Server', () => {
  it('can register a new user', async () => {
    const response = await request(server).post('/api/v1/users/register').send({
      "username": "John",
      "password": "johnscoolpassword"
  })
    expect(response.statusCode).toBe(200)
  })
  
  it('user try register as admin, return err 401', async () => {
    const response = await request(server).post('/api/v1/users/register').send({
      "username": "admin",
      "password": "password"
  })
    expect(response.statusCode).toBe(401)
  })


  it('Returns 404 on invalid address', async () => {
    const response = await request(server).get('/invalidaddress')
    expect(response.statusCode).toBe(404)
  })
})
