import express, { Request, Response } from 'express'
// import { middlewareLogger, errorHandler } from './middleware'
import { errorHandler } from './middleware'


import studentsRouter from './studentRouter'
import userRouter from './userRouter'

const server = express()

server.use(express.json())
// server.use(middlewareLogger)

server.use('/students', studentsRouter)
server.use('/user', userRouter)

server.use(express.static('public'))

server.get('/', (_req: Request, res: Response) => {
  res.send('ok')
})

server.use(errorHandler)

export default server
