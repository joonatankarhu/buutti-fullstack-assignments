import 'dotenv/config'
import express, { NextFunction, Request, Response } from 'express'
import argon from 'argon2'
import jwt from 'jsonwebtoken'

const router = express.Router()

interface User {
  username: string
  password: any
}

const users: Array<User> = []

// // // interface CustomRequest extends Request {
// // //   loginToken?: any
// // }

// // const authenticate = (
//   req: CustomRequest,
//   res: Response,
//   next: NextFunction
// ) => {
//   const auth = req.get('Authorization')

//   if (!auth?.startsWith('Bearer ')) {
//     return res.status(401).send('Invalid token')
//   }

//   const token = auth.substring(7)

//   const secret = process.env.SECRET ?? ''

//   try {
//     const decodedToken = jwt.verify(token, secret)
//     req.loginToken = decodedToken
//     next()
//   } catch (error) {
//     return res.status(401).send('Invalid token')
//   }

//   next()
// }

router.post('/register', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const hash = await argon.hash(password)

  const newUser = {
    username,
    password: hash,
  }

  users.push(newUser)

  const payload = { username: username }
  const secret = process.env.SECRET ?? ''
  const options = { expiresIn: '1h' }

  const userToken = jwt.sign(payload, secret, options)

  res.status(200).send(userToken)
})

router.post('/login', async (req: Request, res: Response) => {
  const reqUsername = req.body.username
  const reqPassword = req.body.password

  const currentUser = users.find((user) => user.username === reqUsername)

  if (currentUser) {
    const verifyPass = await argon.verify(currentUser.password, reqPassword)

    if (verifyPass) {
      const payload = { username: reqUsername }
      const secret = process.env.SECRET ?? ''
      const options = { expiresIn: '1h' }

      const userToken = jwt.sign(payload, secret, options)

      res.status(200).send(userToken)
    }
    if (!currentUser || !verifyPass) {
      res.status(401).send('Unauthorized')
    }
  }

  if (!currentUser) {
    return res.status(401).send()
  }
})

router.post('/admin', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const adminUsername = process.env.ADMIN_USERNAME ?? ''
  const adminPassHash = process.env.ADMIN_PASSWORD_HASH ?? ''
  // admin password in env is mypassword

  const isAdminUsername = username === adminUsername
  const isAdminPass = await argon.verify(adminPassHash, password)

  if (isAdminUsername && isAdminPass) {
    const payload = { username: username }
    const secret = process.env.SECRET ?? ''
    const options = { expiresIn: '1h' }

    const adminToken = jwt.sign(payload, secret, options)
    res.status(200).send(adminToken)
  } else {
    res.status(401).send('Unauthorized')
  }

})

export default router
