import { Request, Response, NextFunction } from 'express'

export const middlewareLogger = (
  req: Request,
  _res: Response,
  next: NextFunction
) => {
  console.log('Request body: ', req.body)

  const time = new Date()
  const method = req.method
  const url = req.url
  console.log({ reqTime: time, reqMethod: method, reqUrl: url })

  next()
}

export const errorHandler = (
  _req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _next: NextFunction
) => {
  res.status(404).send()
}

