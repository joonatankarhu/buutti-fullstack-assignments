import request from 'supertest'
import server from '../src/server'

describe('Server', () => {
  it('Checking if a user can register', async () => {
    const res = await request(server).post('/user/register').send({
      username: 'joona',
      password: 'mypassword',
    })
    expect(res.statusCode).toBe(200)
  })
  it('can login after registering', async () => {
    const res = await request(server).post('/user/login').send({
      username: 'joona',
      password: 'mypassword',
    })
    expect(res.statusCode).toBe(200)
  })
})
