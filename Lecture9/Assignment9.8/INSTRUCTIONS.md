## Assignment 9.8: Securing a Route

Modify the Students API ` /register ` and ` /login ` routes so that on success they return a response with **status code 200** and a **JWT** with username as payload.

Secure all the routes in the *students router* so that they require the user to be logged in to use the routes.

**Extra**: Also modify the ` /admin ` route to return a JWT. Secure the POST, PUT and DELETE routes to require that in addition to being logged in, the user also needs to be an admin.

**Hint**: Here you have some resources on sending a token with Postman.
https://learning.postman.com/docs/sending-requests/authorization/
