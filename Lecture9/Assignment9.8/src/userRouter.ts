import express, { NextFunction, Request, Response } from 'express'
import argon from 'argon2'
import 'dotenv/config'
import jwt from 'jsonwebtoken'

const router = express.Router()

interface User {
  username: string
  password: string
}

interface CustomRequest extends Request {
  username?: string
}

const users: Array<User> = []

const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
  const auth = req.get('Authorization')

  if(!auth?.startsWith('Bearer ')) {
    return res.status(401).send('Invalid token')
  }

  const token = auth.substring(7)

  const secret = process.env.SECRET ?? ''

  try {
    const decodedAdminToken = jwt.verify(token, secret)
    console.log('decodedAdminToken: ', decodedAdminToken)
    // req.username = decodedToken
    next()
  } catch (error) {
    return res.status(401).send('Invalid token')
  }
  
  next()
}

router.post('/', authenticate, async (req: CustomRequest, res: Response) => {
  const { username, password } = req.body

  const payload = { username: username }
  const secret = process.env.SECRET ?? ''
  const options = { expiresIn: '1h' }

  const userToken = jwt.sign(payload, secret, options)

  const hash = await argon.hash(password)

  const newUser = {
    username,
    password: hash,
  }

  users.push(newUser)
  console.log(users)
  res.status(200).send(userToken)
})

router.post('/login', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const user = users.find((user) => user.username === username)

  if (user) {
    const payload = { username: username }
    const secret = process.env.SECRET ?? ''
    const options = { expiresIn: '1h' }

    const userToken = jwt.sign(payload, secret, options)
    const verifyPass = await argon.verify(user.password, password)
    console.log(verifyPass)
    res.status(200).send(userToken)
  }

  if (!user) {
    res.status(401).send()
  }
})

router.post('/admin', async (req: Request, res: Response) => {
  const { username, password } = req.body

  const adminUser = process.env.ADMIN ?? ''
  const adminHash = process.env.ADMIN_HASH ?? ''

  if (username !== adminUser) {
    return res.status(401).send()
  }

  const passVerified = await argon.verify(adminHash, password)
  if (!passVerified) {
    return res.status(401).send()
  }

  const payload = { username: 'admin' }
  const secret = process.env.SECRET ?? ''
  const options = { expiresIn: '1h' }

  const token = jwt.sign(payload, secret, options)

  res.status(200).send(token)
})

export default router
