import jwt from 'jsonwebtoken'

import express, { NextFunction, Request, Response } from 'express'

interface CustomReq extends Request {
  username?: any
}

const router = express.Router()

interface Student {
  id: number
  name: string
  email: string
}

let studentList: Array<Student> = []

const authenticate = (req: CustomReq, res: Response, next: NextFunction) => {
  const auth = req.get('Authorization')
  console.log('step 1')

  if(!auth?.startsWith('Bearer ')) {
    return res.status(401).send('Invalid token')
  }

  const token = auth.substring(7)

  const secret = process.env.SECRET ?? ''

  try {
    const decodedToken = jwt.verify(token, secret)
    req.username = decodedToken
    console.log('step 2')
    
    next()
  } catch (error) {
    return res.status(401).send('Invalid token')
  }
  
  next()
}

router.get('/', authenticate, (req: CustomReq, res: Response) => {
  console.log('step 3')

  const studentId = studentList.map((student: Student) => student.id)
  
  res.send(studentId)
})

router.post('/', authenticate, (req: CustomReq, res: Response) => {
  const { id, name, email } = req.body
  
  const student: Student = {
    id,
    name,
    email,
  }
  studentList.push(student)
  console.log('studentList', studentList)
  res.status(201).send(studentList)
})

router.get('/:id', authenticate, (req: CustomReq, res: Response) => {

  const id = Number(req.params.id)
  const data = studentList.find((stud) => stud.id === id)
  if (data === undefined) {
    res.status(404).send(studentList + 'Error: Student not found.')
  }
  res.send(data)
})

router.put('/:id', authenticate, (req: CustomReq, res: Response) => {

  const { name, email } = req.body
  const id = parseInt(req.params.id)
  const studentIndex = studentList.findIndex((student) => student.id === id)

  if (!studentList[studentIndex]) {
    res.status(404).send()
    return
  }

  if (!name && !email) {
    res.status(400).send('Error: name or email are missing')
  } else if (name || email) {
    if (name) {
      const studentIndex = studentList.findIndex((student) => student.id === id)

      if (studentIndex !== -1) {
        // Checks so the student exists in the array with a arr index. Then updates name to a new name
        studentList[studentIndex] = { ...studentList[studentIndex], name: name }
      } else {
        console.log('Item not found in the array.')
      }
    }
    if (email) {
      const studentIndex = studentList.findIndex((student) => student.id === id)

      if (studentIndex !== -1) {
        // Checks so the student exists in the array with a arr index. Then updates email to a new email
        studentList[studentIndex] = {
          ...studentList[studentIndex],
          email: email,
        }
      } else {
        console.log('Item not found in the array.')
      }
    }
    console.log(`Updated student list: ${studentList}`)

    res.status(204).send()
  }
})

router.delete(':id', authenticate, (req: CustomReq, res: Response) => {
  const id = parseInt(req.params.id)
  const studentIndex = studentList.findIndex((student) => student.id === id)

  if (!studentList[studentIndex]) {
    res.status(404).send()
    return
  }
  const deletedStudentName = studentList[studentIndex].name
  studentList = studentList.splice(studentIndex, 1)
  console.log(`Student ${deletedStudentName} has been deleted from the list.`)
  res.status(204).send()
})

export default router
